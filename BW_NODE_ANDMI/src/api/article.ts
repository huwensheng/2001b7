import { request } from "umi"
export const _get_article = (params = { page: 1, pageSize: 6 }) => request(`/api/api/article`, {
    method: "get",
    params,
    isAuthorization:false,
})

// 删除文章
export const _del_article = (id: string) => request(`/api/api/article/${id}`, {
    method: "delete",
    isAuthorization:false,
})
// 首焦推荐
export const _Recommended_article = (id: string, isRecommended: boolean) => request(`/api/api/article/${id}`, {
    method: "patch",
    data: { isRecommended },
    isAuthorization:false,
})

// //   发布 和 草稿
export const _set_article_status = (id: string, status: string) => request(`/api/api/article/${id}`, {
    method: "patch",
    data: { status },
    isAuthorization:false,
})

//分类管理
export const _get_category=()=>request(`/api/api/category`,{
    method:"get",
    isAuthorization:false,
})