
export interface LoginData {
    name:string,
    password:string
}

export interface BasePageParams {
    page:number,
    pageSize:number,
    name:string,
    email:string,
    role:string,
    status:string
}

export interface Registry {
    name:string,
    password:string
}

export interface Params {
    label:string,
    value:string
}

export interface Updata {
    id:string,
    params:Params
}