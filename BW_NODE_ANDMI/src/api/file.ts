import {request} from "umi"

export const _upload = (body:any)=>request(`/api/api/file/upload`,{
    method:"post",
    data:body
})

export const _get_file = (params = { page: 1, pageSize: 12 ,originalname:'',type:''}) => request(`/api/api/file`, {
    method: "get",
    params,
    isAuthorization:false,
})

//点击选择文件数据
export const _get_know_file = (params = { page: 1, pageSize: 12 }) => request(`/api/api/file`, {
    method: "get",
    params,
    isAuthorization:false
})