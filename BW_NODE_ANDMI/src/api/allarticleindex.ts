import { request } from 'umi';
interface Params {
    page?: number;
    pageSize?: number;
    [propsname: string]: any;
  }
  //页面管理搜索
  export const SearchPageList=(params:Params)=>
    request('/api/api/page',{
      method:"get",
      params,
      isAuthorization:true,
    })
  
  export const commentList = (params: Params) =>
  request('/api/api/article', {
    method: 'get',
    params,
    isAuthorization:false,
  });
  //页面管理
  export const pageList = (params: Params) =>
  request('/api/api/page', {
    method: 'get',
    params,
    isAuthorization:false,
  });
   //知识小册搜索
 export const SearchKnow=(params:Params)=>
 request('/api/api/Knowledge',{
   method:'get',
   params,

   isAuthorization:false,
 })

 export const KnowList=(params:any)=>
 request('/api/api/Knowledge',{
   method:'get',
   params,
   isAuthorization:false,
 })



    
  
