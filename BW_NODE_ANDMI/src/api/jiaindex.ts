import { request } from 'umi';
interface Params {
  page?: number;
  pageSize?: number;
  [propsname: string]: any;
}
// 获取文章数据
export interface allType {
  [propsName: string]: any;
}
export const ArticleList = (params: allType) => {

  return request('/api/api/article', {
    method: 'GET',
    params,
    isAuthorization: false,
  });
};
//获取评论列表
export const commentList = (params: Params) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    isAuthorization: false,
  });
//评论页面删除
export const DelCommentList = (id: any) =>
  request(`/api/api/comment/${id}`, {
    method: 'delete',
    // params,
    isAuthorization: false,
  });
//通过
export const PassCommentList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data,
    isAuthorization: false,
  });
};
//拒绝
export const RefuseCommentList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data,
    isAuthorization: false,
  });
};
// 删除
export const delList = (params: allType) => {
  const { id } = params;
  // console.log(params, 'params');

  return request(`/api/api/search/${params}`, {
    method: 'DELETE',
    isAuthorization: false,
  });
};
//搜索
export const SearchCommentList = (params: Params) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    isAuthorization: false,
  });


//  邮箱数据
export const emailList = () => {
  return request('/api/api/smtp', {
    method: 'GET',
    isAuthorization: false,
  });
};
// // 文件管理数据
export const fileList = (params: any) => {


  return request('/api/api/file', {
    method: 'GET',
    params,
    isAuthorization: false,
  });
};
// 删除文件管理
export const filedel = (params: any) => {
  // console.log(params,'params');
  
  return request(`/api/api/file/${params}`, {
    method: 'DELETE',
    isAuthorization: false,
  });
};

export const postersList = () => {
  return request('/api/api/article', {
    method: 'GET',
    isAuthorization: false,
  });
};

// 海报管理上传
export const postersUpload = () => {
  return request('/api/api/file/upload', {
    method: 'POST',
    isAuthorization: false,
  });
};

// 获取知识小册
export const knowledgeList = () =>
  request('/api/api/knowledge', {
    method: 'get',
    isAuthorization: false,
  });
//知识小册删除
export const DelKnowList = (id: any) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'delete',
    // params,
    isAuthorization: false,
  });
// 搜索
export const searchList = (params:any) => {
  return request('/api/api/search', {
    method: 'GET',
    params,
    isAuthorization: false,
  });
};
