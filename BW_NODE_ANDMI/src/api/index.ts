import { request } from "umi"
import {LoginData} from "./type"
import { BasePageParams, Registry, Params, Updata } from "./type.d"


//页面管理数据渲染
export const get_page = (params:any={page:1,pageSize:12,name:'',path:'',status:''}) => request("/api/api/page", {
    method: "get",
    params,
    isAuthorization: false
})

//知识小册数据渲染
export const _get_konwledge = () => request("/api/api/Knowledge", {
    method: "get",
    isAuthorization: false
})

//访问统计数据渲染
export const _getviewList = (params: any= { page: 1, pageSize: 10 }) => request(`/api/api/view`, {
    params,
    method: "get",
    isAuthorization: false
})
export const postviewList = (params: any) => request(`/api/api/view`, {
    params,
    method: "post",
    isAuthorization: false
})
// 搜索 访问统计
export const SearchviewList = (params: any) =>
    request('/api/api/view', {
        method: 'get',
        params,
        isAuthorization: false
    });
// 访问统计删除
export const Visitlist = (id: any) =>
    request(`/api/api/view/${id}`, {
        method: 'delete',
        // params,
        isAuthorization: false
    });

// 登录账号
export const _login = (data: LoginData) => request(`/api/api/auth/login`, {
    method: "POST",
    data,
    isAuthorization: true//告诉access不需要携带身份标识
})



//echarts图表渲染
export const getChartData = async () => new Promise((resolve) => {
    setTimeout(() => {
        resolve({
            success: true,
            msg: "数据获取成功",
            data: [
                new Array(7).fill("").map(() => Math.floor(Math.random() * 200) + 1),
                new Array(7).fill("").map(() => Math.floor(Math.random() * 200) + 1)
            ]
        })
    }, 100)
})

//分类标签
export const _classfiyList = () =>
    request("/api/api/category", {
        method: 'get',

        isAuthorization: false
    })
//标签管理
export const _viewsList=(params:any)=>
    request("/api/api/tag",{
        method:"get",
        params,
        isAuthorization: false
    })
//添加标签
export const _new_add=(params:Params)=>
    request("/api/api/tag",{
        method:"post",
        data:params,
        isAuthorization:false
    })
//添加分类
export const _new_add_category=(params:Params)=>
    request("/api/api/category",{
        method:"post",
        data:params,
        isAuthorization:false
    })
export const _delete_tag=(id:string)=>
    request(`/api/api/tag/${id}`,{
        method:"delete",
        isAuthorization:false
    })
// 更新标签
export const _updata_label=({id,params}:Updata)=>
    request(`/api/api/tag/${id}`,{
        method:"patch",
        data:params,
        isAuthorization:false
    })



// 注册账号
export const _registry = (data:Registry) => request(`/api/api/user/register`,{
    method:'post',
    data,
    isAuthorization: true
})

export const getnewest = (params: any = { page: 1, pageSize: 6 }) => request(`/api/api/article/recommend`, {
    method: "get",
    params,
    isAuthorization: false
})

// 最新评论 /api/comment
export const getcomment = (params: any = { page: 1, pageSize: 6 }) => request(`/api/api/comment`, {
    method: "get",
    params,
    isAuthorization: false
})

// 回复
export let replymessage = (data: any) => 
     request('api/comment', {
        method: "post",
        data,
        isAuthorization: false
    });

// 获取用户列表
export const _getUserList = (params: BasePageParams = {
    page: 1, pageSize: 10,
    name: "",
    email: "",
    role: "",
    status: ""
}) => request(`/api/api/user`, {
    params,
    isAuthorization: false,
})
//用户页面更新
export const postUserList = (params: any = { item: {} }) =>
    request('/api/api/user/update', {
        method: 'post',
        data: params,
        isAuthorization: false,
        skipErrorHandler: true,
    });


//标签数据获取  get请求
export const _categary = ()=>request(`/api/api/category`,{
    method:'get',
    isAuthorization: false
})

//发布文章管页面
export const PassPageList=(id:string,status:string)=>request(`/api/api/page/${id}`,{
    method:"patch",
    data:{status},
    isAuthorization: false,
})
//分类管理保存
export const ClassfiyList = () => {
    return request('/api/api/category', {
        method: 'POST',
        isAuthorization: false,
    });
}
//分类管理删除
export const ClassfiyDel = (id: string) => 
    request(`/api/api/category/${id}`, {
        method: 'delete',
        isAuthorization: false,
    })
//页面管理删除
export const _del_page=(id:string)=>
    request(`/api/api/page/${id}`,{
        method:'delete',
        isAuthorization: false,
    })

//分类管理更新
export const Classfiyupdate=({id,params}:Updata)=>
    request(`/api/api/category/${id}`,{
        method:'patch',
        data:params,
        isAuthorization: false,
    })

export const _set_pageStatus=(id:string,status:any)=>
   request(`/api/api/page/${id}`,{
    method:'patch',
    data:status,
    isAuthorization: false,
   })

   //通过拒绝  PATCH
export const commentPATCH = (id:number,pass:boolean) =>
request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data:{pass},
    isAuthorization: false
});

// 删除
export const CommentDel = (id: any) =>
request(`/api/api/comment/${id}`, {
    method: 'delete',
    // params,
    isAuthorization: false
});
//知识小册删除
export const Knowdelete=(id:any)=>
    request(`/api/api/knowledge/${id}`,{
        method:'delete',
        isAuthorization: false
    })

//页面
export const _stat_page=(id:string)=>
  request(`/api/api/page/${id}`,{
    method:"get",
    isAuthorization: false,
  })
// 修改信息
export const userdata = (params: any) =>
    request(`/api/api/user/update`, {
        method: "post",
        params,
        isAuthorization: false,
    })

//   密码
// 修改密码
export const userPassword = (params: any) =>
    request(`/api/api/user/password`, {
        method: "post",
        params,
        isAuthorization: false,
    })

// 编辑跳转
export const _to_article_Edit = (id: string) => request(`/api/api/article/${id}`, {
    method: "get",
    isAuthorization: false,
})

// 删除 文章
export const _del_article = (id: string) => request(`/api/api/article/${id}`, {
    method: "delete",
    isAuthorization: false,
})

//标签
export const _get_tag=()=>request(`/api/api/tag`,{
    method:"get",
    isAuthorization: false
})

// 编辑接口
export const _edit_article = (id: string, params: any) => request(`/api/api/article/${id}`, {
    method: "patch",
    data: params,
    isAuthorization: false,
})
  //知识小册发布草稿
  export const know_patch=(id:string,status:any)=>
    request(`/api/api/knowledge/${id}`,{
         method:"patch",
         data:status,
         isAuthorization: false
    })
  


// 新建街口

export const _new_article = (params: any) => request(`/api/api/article`, {
    method: "post",
    data: params,
    isAuthorization: false,
})

//添加
export const _set_addPage = (params: any) => request(`/api/api/page`, {
    method: "post",
    data: params,
    isAuthorization:false,
})


export const _set_pageEditFn = (id:string,params: any) => request(`/api/api/page/${id}`, {
    method: "patch",
    data: params,
    isAuthorization:false,
})