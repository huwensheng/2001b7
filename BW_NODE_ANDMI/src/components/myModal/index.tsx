import { Button, Modal } from 'antd';
import React, { useState } from 'react';
type Props = {
    handleOk: () => void,
    isModalOpen: boolean
}

const MyModal = (props: Props) => {
    const { handleOk, isModalOpen } = props
    console.log(isModalOpen, "isModalOpenisModalOpenisModalOpen");

    return (
        <div>
            <Modal title="访问统计" width={640}  footer={false} open={isModalOpen} onOk={handleOk} onCancel={handleOk}>
              
            </Modal>
        </div>
    )
}

export default MyModal