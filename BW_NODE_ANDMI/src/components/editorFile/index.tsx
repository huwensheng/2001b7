import React, { useState } from 'react'
import { Button, Drawer, Form, Input, Upload, Pagination, message } from 'antd';
import style from "./index.less"
import { useRequest } from 'ahooks';
import { _get_know_file } from '@/api/file';

type Props = {
    onCloseFile: () => void,
    openFile: boolean,
    getImgUrl: any
}

const EditorFile = (props: Props) => {
    const { onCloseFile, openFile, getImgUrl } = props
    // 选择文件数据
    const { data: knowfile, run } = useRequest(_get_know_file)
    const [page, setpage] = useState(1)
    const [pageSize, setpageSize] = useState(12)
    return (
        <Drawer
            width={830}
            title="文件选择"
            placement="right"
            onClose={onCloseFile}
            open={openFile}
        >
            <div>
                <Form
                    name="basic"
                    autoComplete="off"
                >
                    <div className={style.maskfile}>
                        <div className={style.mask_file_search}>
                            <Form.Item name="originalname" label="文件名称" style={{ width: 200 }}>
                                <Input placeholder='请输入文件名称' />
                            </Form.Item>&emsp;
                            <Form.Item name="type" label="文件类型" style={{ width: 200 }}>
                                <Input placeholder='请输入文件类型' />
                            </Form.Item>
                        </div>
                        <div className={style.mask_file_button}>
                            <Button type="primary" htmlType='submit'>搜索</Button>&emsp;
                            <Button htmlType='reset' >重置</Button>
                        </div>
                    </div>
                </Form>
            </div>
            <div className={style.mask_upload}>
                <Upload>
                    <Button>上传文件</Button>
                </Upload>
            </div>
            <div className={style.fileimg_wrap}>
                {
                    knowfile?.data[0].map((item: any, index: number) => {
                        return <div key={index} className={style.file_img} onClick={() => {
                            onCloseFile()
                            getImgUrl(item.url)
                        }}>
                            <div className={style.file_all_img}>
                                <img src={item.url} alt="" />
                            </div>
                            <div className={style.file_all_title}>
                                {item.originalname}
                            </div>
                        </div>
                    })
                }
            </div>
            <div className={style.file_page}>
                <Pagination
                    total={knowfile?.data[1]}
                    showTotal={(total) => `共 ${total} 条`}
                    pageSizeOptions={[8, 12, 24, 36]}
                    defaultCurrent={1}
                    defaultPageSize={12}
                    pageSize={pageSize}
                    current={page}
                    onChange={(page, pageSize) => {
                        setpage(page);
                        setpageSize(pageSize);
                        run({
                            page,
                            pageSize
                        });
                    }}
                />
            </div>
        </Drawer>
    )
}

export default EditorFile