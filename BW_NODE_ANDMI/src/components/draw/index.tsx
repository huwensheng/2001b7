import React, { Component, useState } from 'react';
import styles from "./index.less"
// import type { DrawerProps, RadioChangeEvent } from 'antd';
import { Button, Drawer, Space, Image, message, Popconfirm } from 'antd';
import { useRequest } from 'umi';
import Card from '@/components/fcard/index';
import { filedel, fileList } from '@/api/jiaindex';
import copy from 'copy-to-clipboard';
export default function Index(props: any) {
  // const [visible, setVisible] = useState(false);
  console.log(props, '9999');
  const { run } = useRequest(filedel, { manual: true });

  //打开
  // const showDrawer = () => {
  //     setVisible(true);
  //   };
  //关闭
  const onClose = () => {
    props.changeShowDrawer();
  };

  //   const onChange = (e: RadioChangeEvent) => {
  //     setPlacement(e.target.value);
  //   };
  // 复制
  const copyBtn = (url: string) => {

    copy(url, {
      debug: true,
      message: 'Press #{key} to copy',
    })
    message.success('复制成功');
  }
  // 删除
  const confirm = async (id: string) => {

    message.error('An unknown error');
    await run(id).then((res) => {
      props.changeShowDrawer();

    });
  };

  const cancel = () => {

    message.error('Click on No');
  };
  return (
    <div>
      <Drawer
        title="文件信息"
        placement="right"
        width={650}

        bodyStyle={{ paddingBottom: 80 }}
        onClose={onClose}
        visible={props.visible}
        extra={
          <Space>
            <Button onClick={onClose} style={{ border: 'none' }}>
              X
            </Button>
          </Space>
        }
        footer={
          <div className={styles.footer}>
            <Popconfirm
              placement="topRight"
              className={styles.btn}
              title="Are you sure to delete this task?"
              onConfirm={() => confirm(props.Items.id)}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <Button danger className={styles.btn}>删除</Button>
            </Popconfirm>
            &emsp;
            <Button onClick={onClose} className={styles.btn}>
              关闭
            </Button>

          </div>

        }
      >
        {props.Items &&
          <div className={styles.Itembox}>
            <p><Image
              width={"95%"}
              height={300}
              src={props.Items.url}
            /></p>
            <br />
            <p>文件名称 : <span>{props.Items.originalname}</span></p>
            <p>储存路径 :<span>{props.Items.filename}</span></p>
            <p>文件类型 :<span>{props.Items.type}</span></p>
            <p>文件大小 :<span>{props.Items.size} KB </span></p>
            <p>访问链接 :<span className={styles.spans}>{props.Items.url}</span></p>
            <p><span className={styles.btn} onClick={() => copyBtn(props.Items.url)}>复制</span> </p>
          </div>
        }
      </Drawer>
    </div>
  );
}
