import React, { useEffect, useRef } from 'react';

import { useState } from 'react';

import { Drawer, Form, Button, Col, Row, Input, Switch, Space ,Pagination} from 'antd';

import { PlusOutlined, InboxOutlined ,UndoOutlined} from '@ant-design/icons';
import type { PaginationProps } from 'antd';
import type { UploadProps } from 'antd';
import { message, Upload } from 'antd';
import { fileList } from '../../api/jiaindex';
// import "./antd/es/upload/style"
const { Dragger } = Upload;
const { TextArea } = Input;

import styles from "./index.less"
import { useRequest } from 'umi';

const props: UploadProps = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};


export default function KonwledgeNew() {
  const [visible, setVisible] = useState(false);
  const [visiblee, setVisiblee] = useState(false);
  const [manyword, setManyword] = useState('');
  const { run } = useRequest(fileList, { manual: true });
  const [Data, setData] = useState([]);
    const [DataList, setDataList] = useState([]);
    const [page, setPage] = useState(1)
    const [Items, setItems] = useState({})
    const [pageSize, setPageSize] = useState(10)
    const [total, setTotal] = useState(0)

  const init = async (page: number, pageSize: number) => {
    let res = await run({ page, pageSize })
    // console.log(res, 'res');
    setData(res[0])
    setTotal(res[1])
}
  //选择文件
 
  const onclosee = () => {
    setVisiblee(true);
  };

  const showDrawer = () => {
    setVisible(true);
  };
  

  const onClose = () => {
    setVisible(false);
  };
  const onCloses=()=>{
    setVisiblee(false)
  }

  // 多行文本
  const getManyword = (manyword: any) => {
    console.log(manyword);
  };

  // 开关方法
  const changeSwitch = (checked: any) => {
    console.log(`switch to ${checked}`);
  };
  useEffect(() => {
    init(page, pageSize)

}, []);
//第一个输入框
const textInput = useRef(null) as any;
//第二个输入框
const emailInp = useRef(null) as any;
// 监听分页
const change = (page: number, pageSize: number) => {
    setPage(page)
    setPageSize(pageSize)
    init(page, pageSize)
}
//搜索
const SearchBtn = () => {
  const values = textInput.current.input.value;//文件名称
  const typeValue = emailInp.current.input.value;//文件类型
  run({ page, pageSize, originalname: values, type: typeValue }).then(res => {
      // console.log(res, '搜索res');
      setData(res[0])
      setTotal(res[1])
  })
};
const resetBtn = () => {
  // console.log(seleInp.current);
  textInput.current.input.value = null;
  emailInp.current.input.value = null;
  // init(page, pageSize)
}
const showTotal: PaginationProps['showTotal'] = total => `共 ${total} 条数据`;
  return (
    <div>
      <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />}>
        新建
      </Button>
      <UndoOutlined className={styles.icon} />
      <Drawer
        title="新建知识库"
        width={720}
        onClose={onClose}
        visible={visible}
        // bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={0}>
            <Col span={0} style={{ display: 'flex' }}>
              <Form.Item>名称</Form.Item>
              <Form.Item name="name">
                <Input style={{width:"500px"}} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={0}>
            <Col span={0} style={{ display: 'flex' }}>
              <Form.Item>描述</Form.Item>
              <Form.Item>
                <TextArea
                  value={manyword}
                  onChange={getManyword}
                  placeholder="Controlled autosize"
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  style={{width:"500px"}}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={0} style={{ display: 'flex' }}>
              <Form.Item>评论</Form.Item>
              <Form.Item>
                <Switch defaultChecked onChange={changeSwitch} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={20}>
            <Col span={20} style={{ display: 'flex' }}>
              <Form.Item style={{ width: '50px' }}>封面
              </Form.Item>
              <Form.Item>
                <Dragger>
                   <p className="ant-upload-drag-icon">
                   <InboxOutlined />
                    </p>
                   <p className="ant-upload-text">点击选择文件获将文件拖拽到此处</p>
                   <p className="ant-upload-hint">文件将上传到OSS,如未配置请先配置</p>
                </Dragger>
                                
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}></Col>
          </Row>
        </Form>
        <Row gutter={0}>
            <Col span={0} style={{ display: 'flex' }}>
              <Form.Item name="name">
                <Input style={{width:"500px"}} className={styles.inpt}  />
              </Form.Item>
            </Col>
          </Row>
         <footer >
          <Space>
            <Button onClick={onclosee}  className={styles.closee}>选择文件</Button>
          </Space>
        </footer>
         <div className={styles.btndiv}>
          <p className={styles.p}>
          <Button className={styles.close} onClick={onClose}>取消</Button>
            <Button type="primary">
              创建
            </Button>
          </p>
         </div>
      </Drawer>

      {/* 选择文件 */}
      <Drawer
        title="文件选择"
        width={982}
        onClose={onCloses}
        visible={visiblee}
        // bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col  style={{ display: 'flex' }}>
              <Form.Item>名称</Form.Item>
              <Form.Item name="name" >
                <Input style={{width:"200px"}} placeholder="请输入文件名" ref={textInput}/>
              </Form.Item>
              <Form.Item>文件</Form.Item>
              <Form.Item name="name1" >
                <Input style={{width:"200px"}} placeholder="请输入文件名" ref={emailInp}/>
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <div className={styles.divvs}>
        <p>
          <Button type="primary" onClick={SearchBtn}>搜索</Button>
            &emsp;
            <Button onClick={resetBtn}>
              重置
            </Button>
          </p>
        </div>
        <div className={styles.divupdata}>
        <Button >
              上传文件
            </Button>
        </div>
        <div className={styles.content}>
                    {Data.length &&
                        Data.map((item: any, index: number) => {
                            return (
                                <div key={index} className={styles.dl}>
                            
                                              <img
                                                src={item.url}
                                                alt=""
                                                style={{ width: 207, height: 150 }}
                                            />
                                            <p>{item.originalname}</p>
                                
                                </div>
                            );
                        })}
                    <div className={styles.footer_box}>
                        <div></div>
                        <Pagination
                            showTotal={showTotal}
                            total={total}
                            current={page}
                            pageSize={pageSize}
                            showSizeChanger={true}
                            pageSizeOptions={[10, 20, 50, 100]}
                            onChange={change}
                        />
                    </div>
                </div>
      </Drawer>
    </div>
  );
}
