import { Select } from 'antd'

interface OptionItemType {
    title:string,
    key:string
}

type Props = {
    option:OptionItemType[],
    title?:string,
    onChange?:(value:string)=>void
}


const index = ({option,title="",onChange}: Props) => {
    return (
        <Select placeholder={"请选择"+title}>
            {
                option.map((item:OptionItemType)=>{
                    return <Select.Option onChange={onChange} key={item.key} value={item.key}>
                        {item.title}
                    </Select.Option>
                })
            }
        </Select>
    )
}

export default index