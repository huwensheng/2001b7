import { Drawer, Form, Input, Switch, Select, Button } from 'antd';
import { useEffect, useState } from 'react';
import style from "./index.less"
import { _get_file } from '@/api/file';
import EditorFile from "@/components/editorFile"

const { TextArea } = Input;
const { Option } = Select;

type Props = {
    EditorDrawerData: any,
    formData?: any
}

const EditorDrawer = (props: Props) => {
    const { onClose, open, categoryData, tagData, currentData, } = props.EditorDrawerData
    const [Editorform] = Form.useForm();
    const [formData, setFormData] = useState({})
    const [openFile, setOpenFile] = useState(false)
    const [imgUrl, setImgUrl] = useState("")
    useEffect(() => {
        (() => {
            Editorform.setFieldsValue(currentData)
        })()
    }, [currentData])

    const handleClick = (value: any) => {
        props.formData(value)
    }
    return (
        <>
            <Drawer title="文章设置"
                placement="right"
                onClose={onClose} open={open}
                width="480px"
            >
                <Form
                    name="basic"
                    form={Editorform}
                    onFinish={(values: any) => {
                        onClose()
                        setFormData(values)
                        handleClick({
                            ...values,
                            cover:imgUrl
                        })
                        console.log(values);

                    }}
                    autoComplete="off"
                >
                    <Form.Item label="文章摘要" name="summary">
                        <TextArea rows={4} />
                    </Form.Item>

                    <Form.Item label="访问密码" name="password">
                        <Input.Password placeholder="输入后查看需要密码" />
                    </Form.Item>

                    <Form.Item label="付费查看" name="totalAmount">
                        <Input.Password placeholder="输入需要支付的费用" />
                    </Form.Item>

                    <Form.Item label="开启评论" name="isCommentable">
                        <Switch defaultChecked={currentData.isCommentable} onChange={
                            (checked: boolean) => {
                                console.log(`switch to ${checked}`);
                            }
                        } />
                    </Form.Item>

                    <Form.Item label="首页推荐" name="isRecommended">
                        <Switch defaultChecked={currentData.isRecommended} onChange={
                            (checked: boolean) => {
                                console.log(`switch to ${checked}`);
                            }
                        } />
                    </Form.Item>

                    <Form.Item label="选择分类" name="category">
                        <Select
                            onChange={(value: string) => {
                                console.log(`selected ${value}`);
                            }}>
                            {
                                categoryData && categoryData.data.map((item: any, index: number) => {
                                    return <Option value={item.label} key={index}>{item.label}</Option>
                                })
                            }
                        </Select>
                    </Form.Item>

                    <Form.Item label="选择标签" name="tags">
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}
                            onChange={(value: string[]) => {
                                console.log(`selected ${value}`);
                            }}
                        >
                            {
                                tagData && tagData.data.map((item: any, index: number) => {
                                    return <Option value={item.label} key={index}>{item.label}</Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <div className={style.EditorDrawer_cover}>
                        <span>文章封面</span>
                        <div>
                            <div className={style.EditorDrawer_imgbox} onClick={() => { setOpenFile(true) }}>
                                <img src={imgUrl ? imgUrl : currentData.cover } alt="预览图" />
                            </div>
                            <div>
                                <Form.Item name="cover">
                                    <Input placeholder="或输入外部链接" value={imgUrl ? imgUrl : currentData.cover} />
                                    <Button style={{ marginTop: "10px" }} onClick={() => {
                                        currentData.cover = ""
                                        setImgUrl("")
                                        console.log("移出");
                                    }}>移除</Button>
                                </Form.Item>
                            </div>
                        </div>
                    </div>

                    <Form.Item className={style.submitBtn}>
                        <Button type="primary" htmlType="submit">确认</Button>
                    </Form.Item>
                </Form>

            </Drawer >
            <EditorFile
                openFile={openFile}
                onCloseFile={() => setOpenFile(false)}
                getImgUrl={(e: any) => {
                    setImgUrl(e)
                }}
            ></EditorFile>
        </>
    )
}

export default EditorDrawer