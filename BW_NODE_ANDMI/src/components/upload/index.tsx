import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { message, Upload } from 'antd';
import React from 'react';
import { useRequest } from 'umi';
import { postersUpload } from '../../api/jiaindex';
const { Dragger } = Upload;

const App: React.FC = () => {
  const { run, loading } = useRequest(postersUpload, { manual: true });
  const props: UploadProps = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      // console.log(status);
      run().then((res) => {
        console.log(res), 'run';
      });

      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      // if (status === 'done') {
      //     message.success(`${info.file.name} file uploaded successfully.`);
      // } else if (status === 'error') {
      //     message.error(`${info.file.name} file upload failed.`);
      // }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };

  // console.log(props, 'props');
  return (
    <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
      <p className="ant-upload-text">文件将上传到OSS，如未配置请先配置</p>
    </Dragger>
  );
};

export default App;
