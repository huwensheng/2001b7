import React from 'react';

import { useState } from 'react';

import { Form, Input, Button, Select } from 'antd';

const { Option } = Select;

import style from './index.less';

export default function konwledgeTop() {
  // 名称输入框初始值
  let [title, getTitle] = useState('');
  // 状态输入框初始值
  let [status, getStatus] = useState('');

  // 监听状态输入框
  const handleChange = (value: any) => {
    getStatus(value);
  };
  // 监听名称输入框
  const handleInput = (e: any) => {
    getTitle(e.target.value);
  };

  const passContent = () => {};
  return (
    <div className={style.alltop}>
      <Form>
        <div className={style.alltopone}>
          <Form.Item style={{ fontSize: 16, marginLeft: 30, marginRight: 10 }}>
            名称:
          </Form.Item>
          <Form.Item name="username">
            <Input
              placeholder="请输入知识库名称"
              size="large"
              style={{ width: 255, height: 35, marginRight: 50 }}
              onChange={(e) => handleInput(e)}
            />
          </Form.Item>
          <Form.Item style={{ fontSize: 16, marginRight: 10 }}>状态:</Form.Item>
          <Form.Item>
            <Select
              size="large"
              style={{ width: 200, marginRight: 50 }}
              onChange={(e) => handleChange(e)}
            >
              <Option value="publish">已发布</Option>
              <Option value="draft">草稿</Option>
            </Select>
          </Form.Item>
        </div>
        <div className={style.alltoptwo}>
          <Form.Item>
            <Button
              style={{ marginRight: 10 }}
              size="large"
              type="primary"
              htmlType="submit"
              className="login-form-button"
              onClick={passContent}
            >
              搜索
            </Button>
          </Form.Item>
          <Form.Item>
            <Button size="large">重置</Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
}
