import { Space, Button, Table, Tag } from 'antd';
import React, { FC, useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
// import { DataType } from ".pes"
import style from "./mytable.less"
import { ReloadOutlined } from '@ant-design/icons';
type Props = {
    columns?: any,
    data?: any[],
    change: (e: number, i: number) => void,
}
const MyTable = (props: Props) => {
    const { columns, data, change } = props
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        console.log(newSelectedRowKeys, "newSelectedRowKeys");
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
        <div className={style.myTab}>
            <div className={style.top_box}>
                {
                    hasSelected ? <div>
                        <Button>发布</Button>
                        <Button>草稿</Button>
                        <Button>首焦推荐</Button>
                        <Button>撤销首焦</Button>
                        <Button danger>删除</Button>
                    </div> : <div></div>
                }
                <div>
                    <Button type="primary">+ 新建</Button>
                    <ReloadOutlined />
                </div>
            </div>
            <Table
                pagination={{
                    position: ["bottomRight"],
                    showTotal: () => (`共${data && data.length}条`),
                    showSizeChanger: true,
                    pageSizeOptions: ["3", "12", "24", "26"],
                    onChange: (page, pageSize) => { change(page, pageSize) }
                }}
                columns={columns}
                // scroll={{ x: 300 }}
                dataSource={data}
                rowSelection={rowSelection}
                rowKey={(record) => record.id}
            />;
        </div>
    )
}

export default MyTable