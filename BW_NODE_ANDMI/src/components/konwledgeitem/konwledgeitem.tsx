import React from 'react';

import { Card, Tooltip } from 'antd';

import {
  EditOutlined,
  CloudUploadOutlined,
  SettingOutlined,
  DeleteOutlined,
} from '@ant-design/icons';

const { Meta } = Card;

export default function KonwledgeItem(props: any) {
  let { list } = props;
  return (
    <div
      style={{
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: '10px',
      }}
    >
      {list && list.length > 0
        ? list.map((item: any, index: any) => {
            return (
              <Card
                key={index}
                style={{
                  width: 270,
                  height: 260,
                  marginLeft: 20,
                  marginRight: 8,
                  marginBottom: 20,
                }}
                cover={
                  <img
                    alt="example"
                    src={item.cover}
                    style={{
                      borderTopLeftRadius: 4,
                      borderTopRightRadius: 4,
                      height: 120,
                    }}
                  />
                }
                actions={[
                  <EditOutlined key="edit" />,
                  <Tooltip title="上线">
                    <CloudUploadOutlined key="cloud" />
                  </Tooltip>,
                  <SettingOutlined key="setting" />,
                  <DeleteOutlined key="delete" />,
                ]}
              >
                <Meta
                  title={item.title}
                  description={item.summary}
                  style={{ padding: 24 }}
                />
              </Card>
            );
          })
        : null}
    </div>
  );
}