import React from 'react';

import { useState } from 'react';

import { Drawer, Form, Button, Col, Row, Input, Switch, Space } from 'antd';

import { PlusOutlined, InboxOutlined ,UndoOutlined,SettingOutlined} from '@ant-design/icons';

// import "./antd/es/upload/style"

const { TextArea } = Input;
import styles from "./index.less"

// const { Dragger } = Upload;

export default function KonwledgeNew() {
  const [visible, setVisible] = useState(false);

  const [manyword, setManyword] = useState('');

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  // 多行文本
  const getManyword = (manyword: any) => {
    console.log(manyword);
  };

  // 开关方法
  const changeSwitch = (checked: any) => {
    console.log(`switch to ${checked}`);
  };

  return (
    <div>
      <SettingOutlined onClick={showDrawer} key="setting" />
    
      <Drawer
        title="更新知识库"
        width={720}
        onClose={onClose}
        visible={visible}
        // bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>名称</Form.Item>
              <Form.Item name="name">
                <Input className={styles.inp}/>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>描述</Form.Item>
              <Form.Item>
                <TextArea
                  value={manyword}
                  onChange={getManyword}
                  placeholder="Controlled autosize"
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  className={styles.inp}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>评论</Form.Item>
              <Form.Item>
                <Switch defaultChecked onChange={changeSwitch} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>封面</Form.Item>
              <Form.Item>
                {/* <Dragger>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                    <p className="ant-upload-hint">
                                        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                        band files
                                    </p>
                                </Dragger>, */}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}></Col>
          </Row>
        </Form>
        <footer >
          <Space>
            <Button onClick={onClose}>取消</Button>
            <Button onClick={onClose} type="primary">
              创建
            </Button>
          </Space>
        </footer>
      </Drawer>
    </div>
  );
}