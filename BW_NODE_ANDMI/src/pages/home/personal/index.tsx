import React, { useRef } from 'react'
import styles from './index.less'
import { Tabs, Avatar, Button, Checkbox, Form, Input, message } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { commentList, fileList } from '@/api/jiaindex'
import { _viewsList, _classfiyList, userdata, userPassword } from "@/api"
// import {commentList} from '@/api/allarticleindex'
import { useRequest } from "ahooks"
type Props = {}

// 个人中心

const index = (props: Props) => {
    const { loading: commentLoading, data: commentData } = useRequest(commentList)
    const { loading: fileLoading, data: fileData } = useRequest(fileList)
    const { loading: labelLoading, data: labelData } = useRequest(_viewsList)
    const { loading: classifyLoading, data: classifyData } = useRequest(_classfiyList)

    console.log(JSON.parse(localStorage.getItem("userInfo") as string));
    // 保存
    // CommentModel
    // const {dispatch, namevalue, email, userinfo}=props
    // const save=async (values:any)=>{

    //     const resUser=await userdata(values.name)
    //     const resEmail=await userdata(values.email)
    //     if(resUser.statusCode===200&&resEmail.statusCode===200){
    //         message.success('更改成功');
    //         // userdata(values.id,values.role)
    //     }else{
    //         message.error("更改失败")
    //     }
    // }
    const nameval: any = useRef()
    const emailval: any = useRef()
    // 保存
    const save = () => {
        const params = {
            label: nameval.current.value,
            value: emailval.current.value
        }
        userdata(params).then((res: any) => {
            if (res.success) {
                message.success('更改成功');
                nameval.current.value = null
                emailval.current.value = null
                location.reload()
            } else {
                message.error("更改失败")
            }
        })
    }


    // 密码
    const onFinish = (values: any) => {
        // const {dispatch,password}=props  
        if (values.newPassword && values.connewPassword) {
            if (values.newPassword === values.connewPassword) {
                // dispatch!({
                //     type:"comment/uppassword",
                //     payload:{oldPassword:password,newPassword:values.newPassword}
                //     }
                // )

            }
        }
    };
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <div className={styles.personal_header}></div>
            <div className={styles.personal}>
                {/* 系统概览 */}
                <div className={styles.personal_left}>
                    <div className={styles.system}>
                        <div>系统概览</div>
                        <p>累计发表了 3 篇文章</p>
                        <p>累计创建了 {classifyData?.data.length} 个分类</p>
                        <p>累计创建了 {labelData?.data.length} 个标签</p>
                        <p>累计上传了 {fileData?.data[1]} 个文件</p>
                        <p>累计获得了 {commentData?.data[1]} 个评论</p>
                    </div>
                </div>
                <div className={styles.personal_right}>
                    <div className={styles.datum}>
                        <div className={styles.personal_data}>个人资料</div>
                        <Tabs defaultActiveKey="1" className={styles.personal_set}>
                            <Tabs.TabPane tab="基本设置" key="1">
                                <p className={styles.personal_profile}>
                                    {/* <Avatar size={54} /> */}
                                    <div className={styles.personal_img}></div>
                                </p>
                                <div>
                                    <Form
                                        name="basic"
                                        labelCol={{ span: 8 }}
                                        wrapperCol={{ span: 16 }}
                                        initialValues={{ remember: true }}
                                        onFinish={save}
                                        onFinishFailed={onFinishFailed}
                                        autoComplete="off"

                                    >
                                        <Form.Item
                                            label="用户名"
                                            name="name"
                                            className={styles.personal_username}
                                        >
                                            <Input defaultValue={JSON.parse(localStorage.getItem("userInfo") as string).name} ref={nameval} />
                                        </Form.Item>

                                        <Form.Item
                                            label="邮箱"
                                            name="email"
                                            className={styles.personal_email}
                                        >
                                            <Input defaultValue={JSON.parse(localStorage.getItem("userInfo") as string).email} ref={emailval} />
                                        </Form.Item>
                                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                            <Button type="primary" htmlType="submit" className={styles.save}>
                                                保存
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </div>
                            </Tabs.TabPane>
                            {/* 密码 */}
                            <Tabs.TabPane tab="更新密码" key="2" className={styles.personal_pwd}>
                                <Form
                                    name="basic"
                                    labelCol={{ span: 8 }}
                                    wrapperCol={{ span: 16 }}
                                    initialValues={{ remember: true }}
                                    onFinish={onFinish}
                                    onFinishFailed={onFinishFailed}
                                    autoComplete="off"
                                >
                                    <Form.Item
                                        label="原密码"
                                        name="oldPassword"
                                        className={styles.personal_Password}
                                    >
                                        {/* defaultValue={password} */}
                                        <Input.Password />
                                    </Form.Item>
                                    <Form.Item
                                        label="新密码"
                                        name="newPassword"
                                        className={styles.personal_Password}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                    <Form.Item
                                        label="确定密码"
                                        name="connewPassword"
                                        className={styles.personal_Password}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                    <div>
                                        <Button type="primary" htmlType="submit" className={styles.pwd}>
                                            更改
                                        </Button>
                                    </div>
                                </Form>
                            </Tabs.TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default index 