import styles from './index.less';
import { useRequest } from 'umi';
import { message } from 'antd';

import { useState, FC, useEffect, useRef } from 'react';
import Breadcrumb from '@/components/breadcrumb';
import { searchList, delList } from '@/api/jiaindex';
import { Table, Button, Input, Badge, PaginationProps } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import type { ColumnsType } from 'antd/lib/table';
import Popconfirm from '@/components/Popconfirm/index';
interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
    views: number;
    title: string;
    createAt: string;
    count: number;
    keyword: string;
}
interface PaginationType {
    current: number | undefined | string | boolean,
    pageSize: number,
    showSizeChanger: boolean
}



const IndexPage: FC = () => {
    const columns: ColumnsType<DataType> = [
        {
            title: '搜索词',
            width: 100,
            dataIndex: 'keyword',
            key: 'keyword',
            fixed: 'left',
        },

        {
            title: '搜索量',
            dataIndex: 'count',
            key: 'count',
            width: 150,
            render: (text, record) => <span className={styles.numbers}>{text}</span>,
        },
        {
            title: '搜索时间',
            dataIndex: 'createAt',
            key: 'createAt',
            width: 150,
        },

        {
            title: '操作',
            key: 'operation',
            fixed: 'right',
            width: 100,
            align: 'center',
            render: (text, record) => {
                return (

                    <Popconfirm text={text} page={page} pageSize={pageSize} init={() => init(page, pageSize)}>
                        {/* <a href="">删除</a> */}
                    </Popconfirm>
                );
            },
        },
    ];
    const [pageSize, setPageSize] = useState(5);
    const [page, setPage] = useState(1);
    const [total, setTotal] = useState(0);
    const [Data, setData] = useState([]);
    const [Flag, setFlag] = useState(false);

    //第一个输入框
    const typeInput = useRef(null) as any;
    //第二个输入框
    const seachInp = useRef(null) as any;
    //   第二个输入框
    const numInp = useRef(null) as any;
    // 删除
    const delData = useRequest(delList, { manual: true });
    // 调取数据
    const { run } = useRequest(searchList, { manual: true });
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const init = async (page: number, pageSize: number) => {
        await run({ page, pageSize }).then((res) => {
            // console.log(res, 'res');

            setData(res[0]);
            setTotal(res[1]);
        })
        setFlag(false)

    }
    // const hasSelected = selectedRowKeys.length > 0;
    // function onSelectChange(newSelectedRowKeys: React.Key[]) {
    //     console.log('selectedRowKeys changed: ', selectedRowKeys);
    //     setSelectedRowKeys(newSelectedRowKeys);
    // }
    const onSelectChange = (selectedRowKeys: any) => {
        setSelectedRowKeys(selectedRowKeys);
        setFlag(selectedRowKeys.length > 0);

        // console.log(selectedRowKeys);

        // console.log(selectedRowKeys,"1",selectedRows,"2",info,"3");

    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const changes = (pagination: any) => {
        const { current, pageSize } = pagination;
        // console.log(current, pageSize);
        init(current, pageSize)
        setPage(current);
        setPageSize(pageSize);

    }
    const Alldel = () => {
        for (let index = 0; index < selectedRowKeys.length; index++) {
            delData.run(selectedRowKeys[index]).then(() => {
                init(page, pageSize);
            });
        }
        message.success('删除成功');
        setSelectedRowKeys([]);
        setFlag(false);
    }
    // 搜索
    const searchBtn = () => {
        const typeValue = typeInput.current.input.value;//类型
        const seachValue = seachInp.current.input.value;//搜索词keyword
        const numinpValue = numInp.current.input.value;//搜索量count
        run({ page, pageSize, count: numinpValue, keyword: seachValue, type: typeValue }).then((res) => {
            console.log(res, 'res');
            setData(res[0]);
            setTotal(res[1]);
        })
        setFlag(false)
    }

    // 重置
    const resetBtn = () => {
        typeInput.current.input.value = null;
        seachInp.current.input.value = null;
        numInp.current.input.value = null;
        // init(page, pageSize)
    }

    useEffect(() => {
        init(page, pageSize);
        setFlag(selectedRowKeys.length > 0);
    }, [])
    const showTotal: PaginationProps['showTotal'] = (total) => `共 ${total} 条`;
    return (
        <div>
            <Breadcrumb children={{ title: '搜索记录', path: '/search' }} />
            <div className="head">
                <div className={styles.header_box}>
                    <div className={styles.input_box}>
                        类型:
                        <Input placeholder="请输入搜索类型" style={{ width: '230px' }} ref={typeInput} />
                        搜索词:
                        <Input placeholder="请输入搜索词" style={{ width: '230px' }} ref={seachInp} />
                        搜索量:{' '}
                        <Input placeholder="请输入搜索量" style={{ width: '230px' }} ref={numInp} />
                    </div>
                    <div className={styles.button_box}>
                        <Button type="primary" style={{ marginRight: '30px' }} onClick={searchBtn}>
                            搜索
                        </Button>
                        <Button onClick={resetBtn}>重置</Button>
                    </div>
                </div>
            </div>
            <div className={styles.footer}>
                {Flag ? <Button type="primary" className={styles.btn} onClick={Alldel}>删除</Button> : ""}
                <div className={styles.ioo}>
                    <ReloadOutlined />
                </div>
                <Table
                    rowSelection={rowSelection}
                    rowKey="id"
                    style={{ marginTop: '20px' }}
                    columns={columns}
                    dataSource={Data}

                    scroll={{ x: 1500 }}
                    onChange={changes}
                    pagination={{
                        total,
                        pageSize,
                        pageSizeOptions: [5, 10, 20, 50],
                        showSizeChanger: true,
                        showTotal
                    }}
                />
            </div>
        </div>
    );
}


export default IndexPage