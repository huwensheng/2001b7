
import React, { useEffect, useState } from 'react'
import { PlusOutlined, ReloadOutlined } from '@ant-design/icons';
import styles from './index.less';
import { Affix } from 'antd';
import { history } from 'umi';
import { Button, Table, Badge, message, Form, Input, Popconfirm, Select } from 'antd';
import { get_page, _del_page, _set_pageStatus,_stat_page } from '@/api/index';
type Props = {}
const Index = (props: Props) => {
  const [form] = Form.useForm()
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([])
  const [selectedRows, setSelectedRows] = useState<any>([])
  const [newpageData, setnewpageData] = useState([])
  const my_state = (str: any) => {
    switch (str) {
      case "publish":
        return "success"
      case "draft":
        return "warning"
      default:
        break;
    }
  }

  const list = async ({ page, pageSize, name, path, status }: any) => {
    let res = await get_page({ page, pageSize, name, path, status })
    console.log(res.data[0]);

    setnewpageData(res.data[0])
  }
  useEffect(() => {
    list({});
  }, [])

  const onFinish = async ({ path, status, name }: any) => {
    console.log('success', path, name, status);
    await list({ name, path, status })
  }
  console.log(newpageData);
  const delFn = async (id: string) => {
    let res = await _del_page(id)
    location.reload()
    if (res.statusCode === 200) {
      message.success('操作成功');
    } else {
      message.error('操作失败');
    }
  };
  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      setSelectedRowKeys(selectedRowKeys)
      setSelectedRows(selectedRows)
      // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };


  const columns: any = [
    {
      title: '名称',
      align:"center",
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '路径',
      dataIndex: 'path',
      key: 'path',
      align:"center",
    },
    {
      title: '顺序',
      dataIndex: 'order',
      key: 'order',
      align:"center",
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      align:"center",
      key: 'views',
      render: (_: any, record: any) => (
        <span className={styles.views}>
          {record.views}
        </span>
      ),
    },
    {
      title: '状态',
      align:"center",
      dataIndex: 'status',
      key: 'status',
      render: (_: any, record: any) => (

        <span>
          <Badge status={my_state(record.status)} />
          {record.status == 'publish' ? '已发布' : '草稿'}
        </span>

      ),

    },
    {
      title: '发布时间',
      align:"center",
      dataIndex: 'createAt',
      key: 'createAt',
      render: (_: any, record: any) => (
        <span>
          {new Date(record.createAt).toLocaleString()}
        </span>
      ),
    },
    {
      title: '操作',
      key: 'action',
      align:"center",
      render: (_: any, record: any) => (
        <div>
          <Button type="link" style={{ padding: "0", fontSize: "14px" }}>编辑</Button>&emsp;
          {
            record.status === "draft"
              ? <Button type="link" onClick={() => {
                
                _set_pageStatus(record.id, {
                  status: "publish"
                  
                })
                
              }}>发布</Button>
              : <Button type="link" onClick={() => {
               
                _set_pageStatus(record.id, {
                  status: "draft"
                })
                
              }}>下线</Button>
          }
          <Button type="link" style={{ padding: "0", fontSize: "14px" }}>查看访问</Button>&emsp;
          <Popconfirm title="确定删除这条数据?" okText="确定" cancelText="取消"
            onConfirm={(() => delFn(record.id))}>
            <Button type="link" style={{ padding: "0", fontSize: "14px" }}>删除</Button>
          </Popconfirm>
        </div>
      ),
    },
  ];

  //添加
  return (
    <div className={styles.Page}>
      <Affix offsetTop={48}>
        <div className={styles.Page_header}>
          <span onClick={(() => history.push('/home/workbench'))}>工作台</span> <span>/ </span>  <span>页面管理</span>
        </div>
      </Affix>
      <div>
        <Form className={styles.header}
          form={form}
          onFinish={onFinish}>
          <Form.Item name="name" label="名称">
            <Input placeholder='请输入文章标题' style={{ width: "180px" }} />
          </Form.Item>
          <Form.Item name="path" label="路径">
            <Input placeholder='请输入文章标题' style={{ width: "180px" }} />
          </Form.Item>
          <Form.Item name="status" label="状态">
            <Select style={{ width: "100px" }}>
              <Select.Option value="publish">已发布</Select.Option>
              <Select.Option value="draft">草稿</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item className={styles.lastItem}>
            <Button type="primary" htmlType="submit" >搜索</Button>

            <Button onClick={(() => {
              form.resetFields()
            })}>重置</Button>
          </Form.Item>
        </Form>
        <div className={styles.table}>
          <div className={styles.table_ation}>
            {
              selectedRowKeys.length >= 1 ? <div>
                <Button key="roload" style={{ margin: "0 5px" }} onClick={() => {
                  console.log(selectedRows)
                  selectedRows.forEach((item: any) => {
                    _set_pageStatus(item.id, {
                      ...item,
                      status: "publish"
                    })
                    // location.reload()
                  })
                }}>发布</Button>
                <Button onClick={() => {
                  selectedRows.forEach((item: any) => {
                    _set_pageStatus(item.id, {
                      ...item,
                      status: "draft"
                    })
                    // location.reload()
                  })
                }}>下线</Button>
                <Popconfirm title="确定删除吗" okText="确定" cancelText="取消"
                  onConfirm={() => {
                    selectedRowKeys.forEach((item: any) => {
                      _del_page(item)
                      location.reload()
                    })
                  }}>
                  <Button key="roload" danger style={{ margin: "0 5px" }} >删除</Button>
                </Popconfirm> </div> : <div></div>
            }
            <div > 
              <Button type="primary"
                icon={<PlusOutlined />} onClick={(()=>{
                  history.push("/create/page")
                })}
              >
              新建
              </Button> <ReloadOutlined /></div>
            </div>
          <Table columns={columns} dataSource={newpageData} scroll={{ x: 1200 }} rowSelection={{
            // type: selectionType,
            ...rowSelection,
          }} rowKey={(record) => record.id}
            pagination={{
              defaultPageSize: 7,
              pageSizeOptions: [5, 10, 15, 20],
              showQuickJumper: true,
              showSizeChanger: true,

            }} />
        </div>
      </div></div>
  )

}

export default Index

