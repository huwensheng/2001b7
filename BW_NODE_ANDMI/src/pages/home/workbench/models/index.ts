import {getcomment} from '@/api'

const workCommModel={

    namespace:"workCommModel",
    state:{
        commentData:[]
    },
    reducers:{
        get_Comment(state:any,{payload}:any){
            return{
                ...state,
                commentData:[...payload]
            }
        }
    },
    effects:{
        *get_CommFiles({payload}:any,{put,call}:any){
            const data:{data:any[]}=yield call(getcomment)
            yield put({
                type:"get_Comment",
                payload:[...data.data]
            })
        }
    }

}

export default workCommModel