import styles from "./index.less"
import { FC, useState, useRef } from 'react';
import BaseChart from "@/components/baseChart"
import { useRequest } from "ahooks"
import { getChartData, getnewest, getcomment, CommentDel, commentPATCH } from '@/api'
import { Affix, Button, Card, Popover, Badge, message, Space, Popconfirm, Modal, Input } from 'antd';
const text = <span>页面预览</span>;
const content = (
    <div id={styles.workbench_commentListPageTable}>
        <p>
            <span><b>404</b></span>
            <span className="workbench_content"> This page could not be found.</span>
        </p>

    </div>
);
const { TextArea } = Input;
type Props = {}
// 工作台页面
const WorkBench: FC = () => {

    const [comdata, setComdata] = useState({
        list: [],
        total: 0,
    });

    const { loading, data }: any = useRequest(getChartData, {
        pollingInterval: 60000
    })
    // 最新文章
    const { loading: articleLoading, data: articleData } = useRequest(getnewest)
    // console.log(articleData, 5555);
    // 最新评论
    const { loading: commentLoading, data: commentData }:any = useRequest(getcomment)

    const commentDel = useRequest(CommentDel, { manual: true })
    const arr: any = []
    // 通过
    const success = async (id: number, pass: boolean) => {
        // console.log(id, pass);
            const res = await commentPATCH(id, true)
            if (res.statusCode === 200) {
                location.reload()
                message.success('评论已通过');
            } else {
                message.error('通过失败')


        }
    };
    //   拒绝
    const refuse = async (id: number, item: boolean) => {
        // console.log(id, item);
            const res = await commentPATCH(id, false)
            if (res.statusCode === 200) {
                location.reload()
                message.success('评论已拒绝');
            } else {
                message.error('拒绝失败')
        }
    };


    // 删除
    const confirm = async (id: string) => {
            const res = await CommentDel(id)
            if (res.statusCode === 200) {
                location.reload()
            message.success('操作成功');
            } else {
                message.error('操作失败')
            }
    };
    const cancel = () => {
    };
    // 回复
    const [isModalOpen, setIsModalOpen] = useState(false);
    const textInput = useRef(null) as any;
    // 
    const showModal = () => {
            setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    // 获取到用户信息
    // console.log(JSON.parse(localStorage.getItem("userInfo") as string));


    return (
        <div className={styles.WorkBench}>

            <Affix offsetTop={48} onChange={affixed => console.log(affixed)}>
                <div className={styles.WorkBench_header}>
                    <p><a href="/home/workbench" className={styles.WorkBench_router}>工作台</a></p>
                    <h2 className={styles.WorkBench_router_name}>
                        <b>
                            您好,
                            {JSON.parse(localStorage.getItem("userInfo") as string).name}
                        </b>
                    </h2>
                    <div className={styles.WorkBench_role}>
                        <p>您的角色 :{
                            JSON.parse(localStorage.getItem("userInfo") as string).role === "visitor" ? "访客" : "管理员"
                        }</p>
                    </div>
                </div>
            </Affix>

            {/* 面板导航 */}
            <div className={styles.WorkBench_echart}>
                <div>面板导航</div>
                {/* type用来指定echarts图表的类型 */}
                
               <div className={styles.WorkBench_echart_chart}>
               <BaseChart
                    type="line"
                    chartData={{
                        title: {
                            subtext: "",
                            text: "每周访问指标",

                        },
                        legend: {
                            type: "plain"
                        },
                    }}
                    series={
                        [
                            {
                                type: "bar",
                                name: "评论数",
                                data: data?.data[0],
                                itemStyle: {
                                    color: '#a10f0f',
                                }
                            },
                            {
                                type: "line",
                                name: "访问量",
                                data: data?.data[1],
                                itemStyle: {
                                    color: '#666',
                                }
                            },
                        ]
                    }
                    xData={['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']}
                    loading={loading}
                    width={1025}
                    height={300}
                />
               </div>
            </div>
            {/* 快速导航 */}
            <div className={styles.WorkBench_nav}>
                <p>快速导航</p>
                <div>
                    <a href="/home/acticle/" className={styles.WorkBench_router}>文章管理</a>
                    <a href="/home/comment" className={styles.WorkBench_router}>评论管理</a>
                    <a href="/home/file" className={styles.WorkBench_router}>文件管理</a>
                    <a href="/home/user" className={styles.WorkBench_router}>用户管理</a>
                    <a href="/home/visit" className={styles.WorkBench_router}>访问管理</a>
                    <a href="/home/system" className={styles.WorkBench_router}>系统管理</a>
                </div>
            </div>
            {/* 最新文章 */}
            <div className={styles.workbench_newest}>
                {/* 最新文章 */}
                <div className={styles.workbench_newestarticle}>
                    <p>最新文章</p>
                    <a href="/home/article">全部文章</a>
                </div>
                {/* 图片 */}
                <div className={styles.workbench_newestimg}>
                    {
                        articleData?.data.map((item: any) => {
                            return <Card
                                key={item.id}
                                style={{ height: 280 }}
                                loading={articleLoading}
                                hoverable
                                className={styles.workbench_newestimg_Card}>
                                {
                                    item.cover ?
                                        <img alt="example" src={item.cover} style={{ width: '100%', height: 200 }} />
                                        : <img alt="文章封面" src='' style={{ width: '100%', height: 200 }} />

                                }
                                <p className={styles.workbench_newestimg_title}> {item.title}</p>
                            </Card>
                        })
                    }
                </div>
            </div>
            {/* 最新评论 */}
            <div className={styles.workbench_comment}>
                <div className={styles.workbench_commentarticle}>
                    <p>最新评论</p>
                    <a href="/home/comment">全部评论</a>
                </div>
                <div className={styles.workbench_commentList}>
                    {
                        commentData?.data[0].map((item: any) => {
                            return <div key={item.id}>
                                <p>
                                    <span>{item.name} </span>
                                    <span> 在</span>
                                    {/* 文章 */}
                                    <span>
                                        <Popover placement="right" title={text} content={content}>
                                            <a>文章  </a>
                                        </Popover>

                                    </span>
                                    <span>评论</span>
                                    {/* 查看内容 */}
                                    <span>
                                        <Popover content={item.content} title="评论详情-原始内容">
                                            <a>查看内容</a>
                                        </Popover>
                                    </span>
                                    {/* 通过/未通过 */}
                                    <span className={styles.workbench_commentList_pass}>
                                        {
                                            item.pass ?
                                                <b>
                                                    <Badge status="success" text="通过" />
                                                </b>
                                                :
                                                <b>
                                                    <Badge status="warning" text="未通过" />
                                                </b>
                                        }
                                    </span></p>
                                {/*  通过 拒绝 回复 删除*/}
                                <p className={styles.workbench_commentList_function}>
                                    <a onClick={() => {
                                        success(item.id, item.pass)
                                    }}>通过</a>
                                    <a onClick={
                                        () => {
                                            refuse(item.id, item.pass)
                                        }
                                    }>拒绝</a>
                                    {/* 回复 */}
                                    <a type="primary" onClick={showModal}>回复</a>
                                    {/* 删除确认框 */}
                                    <Popconfirm
                                        title="确认删除这个评论？"
                                        onConfirm={() => {
                                            confirm(item.id)
                                        }}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <a href="#">删除</a>
                                    </Popconfirm>
                                </p>
                                <Modal
                                    title="回复评论"
                                    open={isModalOpen}
                                    onOk={handleOk}
                                    onCancel={handleCancel}
                                >
                                    <TextArea rows={4} placeholder="支持 Markdown" />
                                </Modal>

                            </div>
                        })
                    }
                </div>
            </div>


        </div>
    )
}
export default WorkBench

