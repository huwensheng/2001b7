import React, { useEffect, useState, useRef } from 'react';
import { useRequest } from 'umi';
import {
    commentList,
    DelCommentList,
    PassCommentList,
    RefuseCommentList,
    SearchCommentList,
} from '../../../api/jiaindex';
import Breadcrumb from '@/components/breadcrumb/index';
import {
    Table,
    Space,
    Button,
    Input,
    Select,
    Popover,
    Modal,
    message,
    Popconfirm,
    PaginationProps,
    Form,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { RedoOutlined } from '@ant-design/icons';
import styles from './index.less';
import moment from 'moment';

interface DataType {
    id: string;
    // key: string;
    name: string;
    age: number | string;
    address: string;
    createAt: string;
    hostId: string;
}
const { Option } = Select;
const { TextArea } = Input;
const App: React.FC = () => {
    // const { data, error, loading } = useRequest(commentList);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState({
        total: 0,
        list: [],
    });
    let [page, setpage] = useState(1);
    let [pageSize, setPageSize] = useState(5);
    let [hasSelected, setHasSelected] = useState(false);
    const hasSelectedFlag = () => {
        if (selectedRowKeys.length > 0) {
            setHasSelected(false);
        } else setHasSelected(true);

    }
    // const [nameValue, setNameValue] = useState('');
    // const [EmailValue, setEmailValue] = useState('');
    const [selValue, setSelValue] = useState('');
    const { run, loading } = useRequest(commentList, { manual: true });
    const dellist = useRequest(DelCommentList, { manual: true });
    const passList = useRequest(PassCommentList, { manual: true });
    const UnPassList = useRequest(RefuseCommentList, { manual: true });
    const SearchList = useRequest(SearchCommentList, { manual: true });
    //第一个输入框
    const textInput = useRef(null) as any;
    //第二个输入框
    const emailInp = useRef(null) as any;
    //下拉框
    const seleInp = useRef(null) as any;

    const [isModalVisible, setIsModalVisible] = useState(false);
    //对话框的input框
    const [value, setValue] = useState('');
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    //获取列表数据
    const renderList = async () => {
        let res = await run({ page, pageSize });
        // console.log(res, 'resssss');
        setData({
            list: res[0],
            total: res[1],
        });
        setTotal(res[1])


    };

    // let hasSelected = selectedRowKeys.length > 0 ;
    //管理文章
    const contents = (
        <div className={styles.error_style}>
            <h1>404</h1>{' '}
            <div style={{ fontSize: '30px', paddingLeft: '15px' }}>|</div>
            <h2>This page could not be found</h2>
        </div>
    );
    //表格表头设置
    const columns: ColumnsType<DataType> = [
        {
            title: '状态',
            dataIndex: 'pass',
            key: 'pass',
            fixed: 'left',
            width: '100px',
            render: (text) => (
                <div>
                    {text ? (
                        <div>
                            <span className={styles.span_style}></span>通过
                        </div>
                    ) : (
                        <div>
                            <span className={styles.spans_style}></span>未通过
                        </div>
                    )}
                </div>
            ),
        },
        {
            title: '称呼',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '联系方式',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: '原始内容',
            dataIndex: 'content',
            key: 'content',
            render: (text, record) => {
                return (
                    <Popover
                        content={text}
                        title="评论详情-原始内容"
                        style={{ width: 200, height: '300px' }}
                    >
                        <a>查看内容</a>
                    </Popover>
                );
            },
        },
        {
            title: 'HTML内容',
            dataIndex: 'content',
            key: 'content',
            render: (text, record) => {
                return (
                    <Popover content={text} title="评论详情-HTML内容">
                        <a>查看内容</a>
                    </Popover>
                );
            },
        },
        {
            title: '管理文章',
            dataIndex: 'address',
            key: 'address',
            render: (text, record) => (
                <Popover placement="right" content={contents} title="页面预览">
                    <a href="">文章</a>
                </Popover>
            ),
        },
        {
            title: '创建时间',
            dataIndex: 'createAt',
            key: 'createAt',
            align: 'center',
            render: (text, record) => {
                return (
                    <span>{moment(record.createAt).format('YYYY-MM-DD HH:mm:ss')}</span>
                );
            },
        },
        {
            title: '父级评论',
            dataIndex: 'address',
            key: 'address',
            render: (text) => <div>无</div>,
        },
        {
            title: '操作',
            key: 'action',
            fixed: 'right',
            width: 260,
            render: (text, record) => (
                <Space size="middle">
                    <a
                        onClick={() => {
                            passList
                                .run({
                                    id: record.id,
                                    data: { pass: true },
                                })
                                .then((res) => {
                                    renderList();
                                });
                        }}
                    >
                        通过
                    </a>
                    <a
                        onClick={() => {
                            UnPassList.run({
                                id: record.id,
                                data: { pass: false },
                            }).then((res) => {
                                renderList();
                            });
                        }}
                    >
                        拒绝
                    </a>
                    <a onClick={showModal}>回复</a>
                    <Popconfirm
                        title="确定要删除吗?"
                        onConfirm={() => {
                            confirm(record.id);
                        }}
                        onCancel={cancel}
                        okText="确定"
                        cancelText="取消"
                    >
                        <a href="#">删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];
    //展示总条数
    const showTotal: PaginationProps['showTotal'] = (total) => `共 ${total} 条`;
    useEffect(() => {
        renderList();
    }, []);
    //删除确认
    const confirm = (id: string) => {
        dellist.run(id).then((res) => {
            renderList();
        });
        message.success('删除成功');
        // hasSelectedFlag()
        setHasSelected(false)

    };
    //取消删除
    const cancel = (e: any) => {
        // console.log(e);
        message.error('取消删除');
    };
    //打开对话框
    const showModal = () => {
        setIsModalVisible(true);
    };
    //对话框确认按钮
    const handleOk = () => {
        setIsModalVisible(false);
    };
    // 对话框取消按钮
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //多选
    // function onSelectChange(newSelectedRowKeys: React.Key[]) {
    //     console.log('selectedRowKeys changed: ', selectedRowKeys);
    //     setSelectedRowKeys(newSelectedRowKeys);
    //     console.log(selectedRowKeys);

    // }
    const onSelectChange = (selectedRowKeys: any, selectedRows: any, info: { type: any }) => {
        setSelectedRowKeys(selectedRowKeys);
        if (selectedRowKeys.length > 0) {
            setHasSelected(true);
        } else setHasSelected(false)
        // console.log(selectedRowKeys);

        // console.log(selectedRowKeys,"1",selectedRows,"2",info,"3");

    }
    //全选
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    //批量删除
    const delSome = () => {
        // console.log(selectedRowKeys,'selectedRowKeys')
        selectedRowKeys.forEach((id) => {
            dellist.run(id).then((res) => {
                // console.log(res);
                renderList();
            });
        });
        hasSelectedFlag()
    };
    //批量通过
    const passSome = () => {
        // console.log(selectedRowKeys,'selectedRowKeys')
        selectedRowKeys.forEach((id) => {
            passList
                .run({
                    id,
                    data: { pass: true },
                })
                .then((res) => {
                    renderList();
                });
        });
    };
    //批量拒绝
    const refuseSome = () => {
        selectedRowKeys.forEach((id) => {
            UnPassList.run({
                id,
                data: { pass: false },
            }).then((res) => {
                renderList();
            });
        });
    };
    //搜索
    const SearchBtn = () => {
        const values = textInput.current.input.value;
        const emailValue = emailInp.current.input.value;
        SearchList.run({ page, pageSize, name: values, email: emailValue }).then(
            (res) => {
                console.log(res,'resssssssssssssssss')
                setData({
                    list: res[0],
                    total: res[1],
                });
                setTotal(res[1])

            },
        );
    };
    //下拉
    const handleChange = (value: string) => {
        // console.log(`selected ${value}`);
        setSelValue(value);
        // console.log(selValue, 'selValue');
        SearchList.run({ pass: selValue }).then((res) => {
            setData({
                list: res[0],
                total: res[1],
            });
            setTotal(res[1])

        });
    };
    // 监听分页
    const pageSizes = async (page: number, pageSize: number) => {
        // console.log(page, pageSize);
        setpage(page)
        setPageSize(pageSize)
        let res = await run({ page, pageSize });
        setData({
            list: res[0],
            total: res[1],
        });
        setTotal(res[1])


    }
    // 重置
    const resetBtn = () => {
        // console.log(seleInp.current);
        textInput.current.input.value = null;
        emailInp.current.input.value = null;


    }
    return (
        <div>
            {/* 面包屑 */}
            <Breadcrumb children={{ title: '评论管理', path: '/comments' }} />
            <div className={styles.comment_box}>
                <div className={styles.header_box}>
                    <Form className={styles.input_box}>
                        称呼:
                        <Input
                            placeholder="请输入称呼"
                            style={{ width: '230px' }}
                            ref={textInput}
                        />
                        Email:
                        <Input
                            placeholder="请输入联系方式"
                            style={{ width: '230px' }}
                            ref={emailInp}
                        />
                        状态:{' '}
                        <Select
                            defaultValue=""
                            style={{ width: 200 }}
                            ref={seleInp}
                            onChange={handleChange}
                        >
                            {/* <Option></Option> */}
                            <Option value="0">已通过</Option>
                            <Option value="1">未通过</Option>

                        </Select>
                    </Form>
                    <div className={styles.button_box}>
                        <Button
                            type="primary"
                            style={{ marginRight: '30px' }}
                            onClick={SearchBtn}
                        >
                            搜索
                        </Button>
                        <Button onClick={resetBtn}>重置</Button>
                    </div>
                </div>
                <div className={styles.content_box}>
                    <div className={styles.content_head}>
                        <div>
                            {' '}
                            <span style={{ marginLeft: 8, width: '600px' }}>
                                {hasSelected ? (
                                    <>
                                        {' '}
                                        <Button onClick={passSome}>通过</Button>{' '}
                                        <Button onClick={refuseSome}>拒绝</Button>{' '}
                                        <Button type="primary" danger ghost onClick={delSome}>
                                            删除
                                        </Button>
                                    </>
                                ) : (
                                    ''
                                )}
                            </span>
                        </div>
                        <div>
                            <RedoOutlined
                                style={{ marginLeft: 14 }}
                                onClick={() => {
                                    renderList();
                                }}
                            />
                        </div>
                    </div>
                    <Table
                        rowSelection={rowSelection}
                        rowKey="id"
                        style={{ marginTop: '20px' }}
                        columns={columns}
                        dataSource={data.list}
                        scroll={{ x: 1500 }}

                        pagination={
                            {
                                onChange: pageSizes,
                                pageSize,
                                pageSizeOptions: [5, 10, 30, 50],
                                showSizeChanger: true,
                                total,
                                showTotal
                            }}
                    />
                </div>
            </div>
            {/* 回复对话框 */}
            <Modal
                title="回复评论"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <TextArea
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                    placeholder="支持Markdown"
                    autoSize={{ minRows: 7, maxRows: 9 }}
                />
            </Modal>
        </div>
    );
};

export default App;
