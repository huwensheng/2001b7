import styles from "./index.less"
import React, { FC, useRef, useState } from 'react'
import "../../../global.css"
import { useRequest } from "ahooks";
import { _getUserList, postUserList } from "@/api/index"
import { ProTable, ActionType } from "@ant-design/pro-components"
import BaseSelect from "@/components/baseSelect"
import { Button } from "antd"
import { message } from 'antd';
import { SearchOutlined } from "@ant-design/icons"

type Props = {}
// 用户管理页面
const UserPage: FC = (props: Props) => {
    // manual:true 就是手动获取数据 需要解构runAsync函数来获取数据  自动获取数据把这个属性去掉就行
    const { runAsync } = useRequest(_getUserList, {
        manual: true
    })
    const [selectUser, setselectUser] = useState([])
    const [pageSize, setpageSize] = useState(12);
    const [page, setpage] = useState(1);
    const [name, setsearchname] = useState('');
    const [email, setsearchemail] = useState('');
    const [role, setsearchrole] = useState('');
    const [status, setsearchstatus] = useState('');
    // 禁用
    const Disenable = async (item: any) => {
        if (item.status === 'active') {
            const res = await postUserList({ ...item, status: 'locked' });
            if (res.statusCode === 200) {
                message.success('操作成功');
                _getUserList({ page, pageSize, name, email, role, status })
            } else {
                message.error('服务器错误');
            }
        } else {
            const res = await postUserList({ ...item, status: 'active' });
            if (res.statusCode === 200) {
                message.success('操作成功')
                _getUserList({ page, pageSize, name, email, role, status })
            } else {
                message.error('服务器错误');
            }
        }
        actionRef.current&&actionRef.current.reload()
    }
    // 授权
    const accredit = async (item: any) => {
        if (item.role === 'admin') {
            const res = await postUserList({ ...item, role: 'visitor' });
            if (res.statusCode === 201) {
                message.success('操作成功');
                location.reload()
                _getUserList({ page, pageSize, name, email, role, status });
            } else {
                message.error('服务器错误');
            }
        } else {
            const res = await postUserList({ ...item, role: 'admin' });
            if (res.statusCode === 201) {
                message.success('操作成功');
                location.reload()
                _getUserList({ page, pageSize, name, email, role, status });
            } else {
                message.error('服务器错误');
            }
        }
        actionRef.current&&actionRef.current.reload()
    }
    const actionRef = useRef<ActionType | undefined>()
    const columns: any = [
        {
            title: '账号',
            dataIndex: 'name'
        },
        {
            title: '邮箱',
            dataIndex: 'email'
        },
        {
            title: '角色',
            dataIndex: 'role',
            renderFormItem: (item: any, { type, defaultRender, ...rest }: any) => {
                // return 封装一个组件
                return <BaseSelect
                    title="角色"
                    {...rest}
                    option={
                        [
                            {
                                title: '访客',
                                key: 'visitor'
                            },
                            {
                                title: '管理员',
                                key: 'admin'
                            }
                        ]
                    }
                />
            }
        },
        {
            title: '状态',
            dataIndex: 'status',
            renderFromItem: (item: any, { type, defaultRender, ...rest }: any) => {
                return <BaseSelect
                    title="状态"
                    {...rest}
                    option={
                        [
                            {
                                title: '已锁定',
                                key: 'lock'
                            },
                            {
                                title: '可用',
                                key: 'active'
                            }
                        ]
                    }
                />
            }
        },
        {
            title: '注册日期',
            dataIndex: 'createAt'
        },
        {
            title: '操作',
            render: (_text: any, record: any) => {
                return <div>
                    <Button onClick={() => Disenable(record)}>
                        {record.status === 'active' ? '禁用' : '启用'}
                    </Button>
                    <Button onClick={() => {
                        accredit(record)
                    }}>
                        {
                            record.role === "visitor" ? "授权" : "解除授权"
                        }
                    </Button>
                </div>
            }
        }
    ]
    // 搜索
    return (
        <ProTable
            rowSelection={
                // 复选框
                {
                    type: 'checkbox'
                }
            }
            actionRef={actionRef}
            columns={columns as any}//列的配置
            rowKey={"id"}
            pagination={{
                current: 1,
                pageSize: 10
            }}
            // 搜索
            search={{
                searchText: "查询",
                span: 5,
                optionRender: (searchConfig, formPros, dom) => {
                    return dom
                }//自定义操作栏
            }}
            toolbar={{
                actions: [
                    <Button key="reload" icon={<SearchOutlined />}></Button>
                ]
            }}
            request={
                async (options) => {//初始化的时候创建表格执行，分页  改变执行 ，点击查询的时候也会执行
                    options = {
                        ...options,
                        page: options.current
                    }
                    delete options.current
                    const { data } = await runAsync(options as any) //手动发起请求
                    return {
                        data: data[0],
                        success: true,
                        total: data[1]
                    }
                }
            }
        ></ProTable>
    )
}

export default UserPage