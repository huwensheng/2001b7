

import React, { useState, FC, useEffect, useRef } from 'react';
import style from './index.less';
import { Popconfirm,Tag } from 'antd';
import { Button, Card, message } from 'antd';
import {  ConnectProps, Loading, connect } from 'umi';
import { history } from 'umi'
import { _viewsList } from "@/api"
import { useRequest } from "ahooks";
import { _delete_tag, _new_add, _updata_label } from "@/api/index"

type Props = {}

const ButtonGroup = Button.Group;

interface PageProps extends ConnectProps {
  tag: any;
  loading: boolean;
}
const Index: FC<PageProps> = ({ tag, dispatch }) => {
 
  let classKey: any = useRef()
  let classValue: any = useRef()
  let [temp, setTemp] = useState(false)
  let [Item, setItem] = useState(null)
  const [id,setId] = useState('')

  const {loading:classfiyLoding,data:classfiyData} :any=useRequest(_viewsList)
  // 回显
  const getEcho = (item: any) => {
    setItem(Item = item)
    setTemp(temp = true)
    classKey.current.value = item.label
    classValue.current.value = item.value
    setId(item.id)
  }
  // 保存
  const getValue = () => {
    const params = {
      label:classKey.current.value,
      value:classValue.current.value
    }
    _new_add(params).then((res:any)=>{
      if(res.success){
        message.success('标签添加成功')
        classKey.current.value = null
        classValue.current.value = null
        location.reload()
      }else{
        message.error('标签添加失败')
      }
    })
  }
  // 返回添加
  const hanldReturn = () => {
    setTemp(temp = false)
    classKey.current.value = null
    classValue.current.value = null

  }
  // 更新
  const hanldupdate = () => {
    const params = {
      label:classKey.current.value,
      value:classValue.current.value
    }
    _updata_label({id,params}).then((res:any)=>{
      if(res.success){
        message.success('更新分类成功')
        classKey.current.value = null
        classValue.current.value = null
        location.reload()
      }else{
        message.error('更新失败')
      }
    })
    setTemp(temp = false)
  }
  // 删除
  const handleDel = () => {
    _delete_tag(id).then((res:any)=>{
      console.log(res);
      if(res.statusCode===200){
        message.success('删除分类成功')
        classKey.current.value = null
        classValue.current.value = null
        location.reload()
      }else if(res.statusCode===403){
        message.error('访客身份没有权限')
      }else{
        message.error('删除失败')
      }
    })
    setTemp(temp = false)
  }
  return (
    <div className={style.tagss}>
                 <div className={style.nav}>
            <p> <span onClick={()=>{
                history.push('/home/workbench')
            }}>工作台</span> / <span onClick={()=>{
                history.push('/home/article')
            }}>所有文章</span> / <span>标签管理</span> </p>
            </div>
      <div className={style.Tage}>
        <Card title={temp ? '管理分类' : '添加分类'} bordered={false} className={style.addtag}>
          <input placeholder="输入标签名称" type="text" ref={classKey} />
          <input placeholder="输入标签值（请输入英文，作为路由使用）" type="text" ref={classValue} />
          {
            temp ?
              <div className={style.tags_update}>
                <ButtonGroup className={style.left}>
                  <Button type="primary" onClick={hanldupdate}>更新</Button>
                  <Button type="dashed" onClick={hanldReturn}>返回添加</Button>
                </ButtonGroup>
                <Popconfirm className={style.right} title="确定删除吗?" onConfirm={handleDel} okText="确定" cancelText="取消">
                  <Button danger ghost>删除</Button>
                </Popconfirm>
              </div>
              : <Button type="primary" htmlType="submit" onClick={getValue}>保存</Button>
          }
                                </Card>
                            <div className={style.alltag}>
                            <div className={style.mainleftclassfiy}> <span>所有标签</span></div>
                            {
                            // classfiyData.data[0]? <p>{ classfiyData.data[0].category.label}</p>:"NoData"
                            classfiyData?.data.map((item:any,index:any)=>{
                                return <Tag onClick={() => getEcho(item)}  className={style.tag} key={index}>{item.label}</Tag>
                            })
                        }
        </div>
        
      </div>
    </div>
  );
}
export default connect((state: any) => {
  return state;
})(Index);