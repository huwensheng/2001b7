import styles from "./index.less"
import {FC} from 'react'
import "../../../global.css"
import { Affix, Breadcrumb } from 'antd';
import type { RadioChangeEvent } from 'antd';
import { Radio, Space, Tabs } from 'antd';
import { useState } from 'react';

import Settings from './systemPage/Settings'
import SEO from './systemPage/seo/seo'
import SMTP from './systemPage/SMTP'
import Country from './systemPage/country'
import OSS from './systemPage/OSS'
import Statistics from './systemPage/Statistics'
// 系统设置页面
type TabPosition = 'left';
const { TabPane } = Tabs;
const index = () => {

  const [tabPosition, setTabPosition] = useState<TabPosition>('left');
  const changeTabPosition = (e: RadioChangeEvent) => {
    setTabPosition(e.target.value);
  };
 
  return (
    <div className={styles.system}>

      <Affix offsetTop={48} onChange={affixed => console.log(affixed)}>
        <Breadcrumb separator="/" className={styles.system_header}>
          <Breadcrumb.Item href="/home/workbench">工作台</Breadcrumb.Item>
          <Breadcrumb.Item href="/home/system">系统设置</Breadcrumb.Item>
        </Breadcrumb>
      </Affix>

      <div className={styles.styles_tabls}>
      <Tabs tabPosition="left">
        <TabPane tab="系统设置" key="1">
          <Settings/>
        </TabPane>
        <TabPane tab="国际化设置" key="2">
        <Country/>
        </TabPane>
        <TabPane tab="SEO设置" key="3">
         <SEO/>
        </TabPane>
        <TabPane tab="数据统计" key="4">
          <Statistics />
        </TabPane>
        <TabPane tab="OSS设置" key="5">
         <OSS/>
        </TabPane>
        <TabPane tab="SMTP服务" key="6">
          <SMTP/>
        </TabPane>
      </Tabs>

      </div>


    </div>
  )
}

export default index