import React, { useState, useEffect } from 'react';
import { Input, Button, Tooltip, } from 'antd';
import { CopyOutlined } from '@ant-design/icons';
import styles from "./index.less"
const { TextArea } = Input;

type Props = {}
// 系统设置
const Settings = (props: Props) => {
    const [system] = useState("www.com");
    const [backstage] = useState("creationadmin.shbwyz.com");
    const [systemtitle] = useState("ikun");
    const [Logo] = useState("https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif");
    const [Favicon] = useState("https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif");
    const [footerMessage] = useState("首页");
    return (
        <div className={styles.setting}>
            <div>
                <p>系统地址</p>
                <Input placeholder="请输入系统地址" defaultValue={system}/>
            </div>
            <div>
                <p>后台地址</p>
                <Input placeholder="请输入后台地址" defaultValue={backstage}/>
            </div>
            <div>
                <p>系统标题</p>
                <Input placeholder="请输入系统标题" defaultValue={systemtitle}/>
            </div>
            <div>
                <p>Logo</p>
                <Input.Group compact>
                    <Input
                        style={{ width: "96%" }}
                        placeholder="请输入logo连接"
                        defaultValue={Logo}
                    />
                        <Button icon={<CopyOutlined />} />
                </Input.Group>
            </div>
            <div>
                <p>Favicon</p>
                <Input.Group compact>
                    <Input
                        style={{ width: "96%" }}
                        placeholder="请输入Favicon连接"
                        defaultValue={Favicon}
                    />
                        <Button icon={<CopyOutlined />} />
                </Input.Group>
            </div>
            <div>
                <p>页脚信息</p>
                <TextArea rows={4} placeholder="请输入页脚信息" defaultValue={footerMessage}/>
            </div>
            {/* 保存 */}
            <Button type="primary" className={styles.setting_save}>保存</Button>
        </div>
    )
}

export default Settings