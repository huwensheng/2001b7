import React, { useState } from 'react'
import { Tabs } from 'antd';
const { TabPane } = Tabs;
import { useRequest } from "ahooks"
// import {getsetting } from '@/api'
const initialPanes = [
    {
        title: 'en', content: "123",
        key: '1'
    },
    {
        title: 'zh', content: "222",
        key: '2'
    },

];
export default function Internationalization() {

    // const { loading: commentLoading, data: commentData } = useRequest(getsetting)
    // console.log(commentData, 222);

    var newTabIndex = 0;
    const [activeKey, setactiveKey] = useState(initialPanes[0].key)
    const [panes, setpanes] = useState(initialPanes)
    function onChange(activeKey: any) {
        setactiveKey(activeKey)
    };
    function add() {
        const activeKey = `newTab${newTabIndex++}`;
        const newPanes = [...panes];
        newPanes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
        setactiveKey(activeKey)
        setpanes(newPanes)
    };

    return (
        <div>
            <Tabs
                type="editable-card"
                onChange={onChange}
                activeKey={activeKey}
            >
                {panes.map(pane => (
                    <TabPane tab={pane.title} key={pane.key}>
                        {pane.content}
                    </TabPane>
                ))}
            </Tabs>
        </div>
    )
}