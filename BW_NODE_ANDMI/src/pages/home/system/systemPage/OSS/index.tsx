// oss
import React, { useState, useEffect } from 'react';
import { Alert } from 'antd';
import styles from "./index.less"


type Props = {}
//oss
const OSSPage = (props: Props) => {

    return (
        <div>
            <div>
                <Alert
                    message="说明"
                    description={`请在编辑器中输入您的 oss 配置，并添加 type 字段区分 {"type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""}`}
                    type="info"
                    showIcon
                />
                <p></p>
            </div>
        </div>
    )
}

export default OSSPage