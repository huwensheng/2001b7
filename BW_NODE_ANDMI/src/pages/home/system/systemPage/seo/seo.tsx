import React, { useState, useEffect } from 'react';
import { Input, Button } from 'antd';
import styles from "./index.less"
import {postviewList} from "@/api"
import { useRequest } from "ahooks"
const { TextArea } = Input;
type Props = {}
//SEO
const SEOPage = (props: Props) => {
    // const {data}=useRequest(postviewList,{manual:true})
    // 关键
    const [crux] = useState(`"JavaScript,TypeScript,Vue.js,微信小程序,React.js,正则表达式,WebGL,Webpack,Docker,MVVM,nginx,java",   "seoDesc": "“小楼又清风”是 fantasticit（https://github.com/fantasticit）的个人小站。本站的文章包括：前端、后端等方面的内容，也包括一些个人读书笔记。"`);
    // 描述
    const [describe] = useState("“小楼又清风”是 fantasticit（https://github.com/fantasticit）的个人小站")


    return (
        <div className={styles.SEOPage}>
            <div className="itemplug">
                <p>关键字</p>
                <Input placeholder="Basic usage" defaultValue={crux} />
            </div>
            <div className="itemplug">
                <p>描述信息</p>
                <TextArea rows={4} defaultValue={describe} />
            </div>
            <div className="itembutton">
                {/* 保存 */}
                <Button type="primary">保存</Button>
            </div>
        </div>
    )
}

export default SEOPage