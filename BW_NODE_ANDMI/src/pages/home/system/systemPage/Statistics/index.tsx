import React, { useState, useEffect } from 'react';
import { Input, Button } from 'antd';
import styles from "./index.less"


type Props = {}
//数据统计
const Statistics = (props: Props) => {
    const [Baidu] = useState("2f616121a4be61774c494d106870f30e");
    const [Google] = useState("G-10SK76KWMS")


    return (
        <div className={styles.Statistic}>
            <div>
                <p>百度统计</p>
                <Input defaultValue={Baidu} />
            </div>
            <div>
                <p>谷歌分析</p>
                <Input defaultValue={Google} />
            </div>
            {/* 保存 */}
            <Button type="primary">保存</Button>
        </div>
    )
}

export default Statistics