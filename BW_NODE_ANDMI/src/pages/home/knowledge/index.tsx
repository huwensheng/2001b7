import styles from "./index.less"
import { EditOutlined, EllipsisOutlined, SettingOutlined, UndoOutlined,CloudUploadOutlined,DeleteOutlined,CloudDownloadOutlined  } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import { useDispatch, useRequest, useSelector } from "umi";
import React, { useState, FC, useEffect, useRef } from 'react';
import { Switch, Table, Button, Popconfirm, Tag, Input, Select, Cascader,Tooltip  } from 'antd';
import KonwledgeNew from '@/components/konwledgetnew/konwledgetnew';
const { Meta } = Card;
import { history } from 'umi';
import {SearchKnow,KnowList} from "../../../api/allarticleindex"
import {_get_konwledge} from "@/api"
import {Knowdelete,know_patch} from "../../../api/index"
import {  Drawer, Radio, Space } from 'antd';
import type { DrawerProps } from 'antd/es/drawer';
import type { RadioChangeEvent } from 'antd/es/radio';
import {  Form, Col, Row } from 'antd';
type Props = {}

// 知识小册页面管理

const KnowLedge: React.FC=() => {
    const dispatch = useDispatch()
    let [page, setpage] = useState(1);
    let [pageSize, setPageSize] = useState(10);
    const { run } = useRequest(KnowList, { manual: true });
    const SearchList = useRequest(SearchKnow, { manual: true });
    const { TextArea } = Input;
    const [visible, setVisible] = useState(false);
    const [manyword, setManyword] = useState('');
    //回显
    let classValue: any = useRef()

    const getManyword = (manyword: any) => {
        console.log(manyword);
      };
    const [data, setData] = useState({
        list: [],
        total: 0,
    });
    const { Option } = Select;
    const { konwledgeList } = useSelector(({ konwModelpage }: { konwModelpage: any }) => { //从仓库拿数据
        return {
            ...konwModelpage
        }
    })
    // console.log(konwledgeList)
    useEffect(() => {
        dispatch({
            type: "konwModelpage/getkonwledge"
        })
    }, [dispatch])
   

    const renderList = async () => {
        let res = await run({ page, pageSize, });
        setData({
          list: res[0],
          total: res[1],
        });
      };
      useEffect(() => {
        renderList();
    }, []);
    //抽屉
    const [open, setOpen] = useState(false);
    const [placement, setPlacement] = useState<DrawerProps['placement']>('right');
    const [drawerList, setdrawerList] = useState([])
    const [drawerform]: any = Form.useForm();
    const [drawerimg,drawerimglist]=useState('')
    //打开抽屉
    const showDrawer = (item:any) => {
        setOpen(true);
        drawerform.resetFields();
        drawerform.setFieldsValue(item)
        setdrawerList(item)
        drawerimglist(item.cover)
      };
    console.log(drawerimg)
      const onChange = (e: RadioChangeEvent) => {
        setPlacement(e.target.value);
      };
    
      const onClose = () => {
        setOpen(false);
      };
    
      const text = <span>prompt text</span>;
    //第一个输入框
    const textInput = useRef(null) as any;
    //搜索
    const searchpage = () => {
        const values = textInput.current.input.value;
        SearchList.run({ page, pageSize, title: values }).then(
          (res: any) => {
            console.log(res)    
                setData({
                    list: res[0],
                    total: res[1],
                  });
          },
        );
    };
    const [isModalVisible, setIsModalVisible] = useState(false);
    //重置
    const ace=()=>{
        const values = textInput.current.input.value;
        renderList()
    }
        // 对话框取消按钮
    const handleCancel = () => {
        setIsModalVisible(false);
    };
   //删除
        const confirm = (id: string) => {
            Knowdelete(id).then((res: any) => {
            if (res.statusCode===200) {
                // console.log(res)
            }
            renderList();
         
        });
    }
    const changeSwitch = (checked: any) => {
        console.log(`switch to ${checked}`);
      };
    const showModal = () => {
        setIsModalVisible(true);
    };
    //对话框确认按钮
    const handleOk = () => {
        setIsModalVisible(false);
    };
    return (
        <div className={styles.allarticleWarp}>
            <div className={styles.nav}>
                <p>
                    <span onClick={() => {
                        history.push('/home/workbench')
                    }}>工作台</span> / 知识小册
                </p>
            </div>
            <div className={styles.main}>
                <div className={styles.search}>
                    {/* 搜索 */}

                    <p className='searchinput'><span className={styles.classfit_span}>分类:</span>  <Input placeholder="请输入文章标题" style={{ width: "200px" }} ref={textInput} /></p>

                    <span className={styles.searchace}>状态:</span><p className={styles.searchinput_title}><Input.Group compact>
                        <Select style={{ width: "250px" }} >
                            <Option value="Home">已发布</Option>
                            <Option value="Company">草稿</Option>
                        </Select>
                    </Input.Group>
                    </p>
                    <p className={styles.searchbtn}>
                        <Button type="primary"  onClick={searchpage}>搜索</Button> <Button type="primary" className={styles.search_btn} onClick={ace} >重置</Button>
                    </p>
                </div>
                <div className={styles.newdiv}>
                    {/* <Button className={styles.button} type="primary">+ 新建</Button><UndoOutlined className={styles.icon} /> */}
                    <KonwledgeNew></KonwledgeNew>
                </div>
            </div>
            <div className={styles.konwledgeListdiv}>
                {
                    data.list.map((item: any, index: any) => {
                        return <div className={styles.konwledgeListdiv_img} key={index}>
                            <Card
                                key={index}
                                style={{ width:270 }}
                                cover={
                                    <img
                                        style={{ height: "120px" }}
                                        alt="example"
                                        src={item.cover}
                                    />
                                }
                                actions={[
                                    <div className={styles.konwledgeListdivicon}>
                                      
                                    <EditOutlined key="edit" className={styles.knowhover} onClick={()=>{
                                             history.push('/home/knowdetails'+item.id)
                                    }}/>
                                    {/* 发布 */}
                                   {
                                    item.status === "draft"
                                      ? 
                                      <Tooltip placement="topLeft" title="发布线上">
                                        <CloudUploadOutlined type="link" onClick={() => {
                                        know_patch(item.id, {
                                          status: "publish"
                                        })
                                      }}>发布</CloudUploadOutlined>
                                        </Tooltip>
                                      
                                      : 
                                      <Tooltip placement="topLeft" title="设为草稿">
                                          <CloudDownloadOutlined type="link" onClick={() => {
                                        know_patch(item.id, {
                                          status: "draft"
                                        })
                                      }}>下线</CloudDownloadOutlined>
                                        </Tooltip>
                                    
                                  }
                                  {/* 设置 */}
                                    <span  onClick={()=>{
                                        showDrawer(item)
                                    }}>
                                    <SettingOutlined  key="setting" />
                                    </span>
                                         <Popconfirm
                                    key={4}
                                    title="确定要删除吗?"
                                    onConfirm={() => {
                                        confirm(item.id)
                                    }}
                                    // onCancel={cancel}
                                    okText="确定"
                                    cancelText="取消"
                                >
                                     <DeleteOutlined key="5555"/>
                                </Popconfirm>
                                    </div>
                                ]}
                            >
                                <Meta
                                style={{height:"50px"}}
                                  
                                    title={item.title}
                                    description={item.summary}
                                />
                            </Card>
                        </div>

                    })
                }
            </div>
            <Space>
       
      </Space>
      <Drawer
        title="更新知识库"
        placement={placement}
        width={500}
        onClose={onClose}
        open={open}
      >
       <Form layout="vertical" hideRequiredMark 
       form={drawerform}
       >
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>名称</Form.Item>
              <Form.Item name="title">
                <Input className={styles.inp}    />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>描述</Form.Item>
              <Form.Item name="summary">
                <TextArea
                  value={manyword}
                  onChange={getManyword}
                 
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  className={styles.inp}
                  ref={classValue}
                />
              </Form.Item>

            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>评论</Form.Item>
              <Form.Item>
                <Switch defaultChecked onChange={changeSwitch} />
              </Form.Item>
            </Col>
          </Row>
          <div className={styles.drawerdiv}>
               {
                 <img src={drawerimg} alt="" className={styles.drawerdivimg}/>
               }
          </div>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item name="cover">
                <Input className={styles.inp}    />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12} style={{ display: 'flex' }}>
              <Form.Item>封面</Form.Item>
              <Form.Item>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}></Col>
          </Row>
        </Form>
        <footer >
          <Space>
            <Button onClick={onClose}>选择文件</Button>
            <Button className={styles.btns} onClick={()=>{
              drawerimglist('')
            }} type="primary">
              移出
            </Button>
          </Space>
        </footer>
         <div className={styles.btndiv}>
          <p className={styles.p}>
          <Button className={styles.close} onClick={onClose}>取消</Button>
          
            <Button type="primary">
              更新
            </Button>
          </p>
         
         </div>
      </Drawer>
        </div>
    )
}

export default KnowLedge