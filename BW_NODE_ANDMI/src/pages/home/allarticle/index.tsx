
import { FC, useEffect, useRef, useState } from 'react'
import { Space, Tag, Badge, Button, Popconfirm,Affix } from 'antd';
import style from "./index.less"
import { useRequest, } from "ahooks"
import { _get_article, _del_article, _Recommended_article, _set_article_status, _get_category } from "@/api/article"
import { ActionType, ProColumns, ProTable } from '@ant-design/pro-components';
import BaseSelect from '@/components/baseSelect';
import {history} from 'umi'
import MyModal from '@/components/myModal';
import { date } from '@/utils/time';
const Article: FC = () => {
  const actionRef = useRef<ActionType | undefined>()
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { runAsync, } = useRequest(_get_article, {
    manual: true,
  })
  // 获取分类数据
  const { data } = useRequest(_get_category)
  let categoryArr: { title: any; key: any; }[] = []
  data?.data.forEach((item: any) => {
    categoryArr.push({
      title: item.label,
      key: item.id,
    })
  });
  const columns: ProColumns[] = [
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
      width: 150,
      fixed: 'left',
      align:"center",
    },
    {
      title: '状态',
      dataIndex: 'status',
      align:"center",
      key: 'status',
      renderFormItem: (item, { type, defaultRender, ...rest }) => {
        return <BaseSelect
          {...rest}
          title="状态"
          option={
            [
              {
                title: "已发布",
                key: "publish"
              },
              {
                title: "草稿",
                key: "draft"
              }
            ]
          }
        ></BaseSelect >
      },
      render: (_: any, record: any) => {

        if (record.status === "publish") {
          return <><Badge status="success" />已发布</>
        }
        if (record.status === "draft") {
          return <><Badge status="warning" />草稿</>
        }
      },
    },
    {
      title: '分类',
      dataIndex: 'category',
      align:"center",
      key: 'category', //'#f50' 
      renderFormItem: (item, { type, defaultRender, ...rest }) => {

        return <BaseSelect
          {...rest}
          title="分类"
          option={categoryArr}
        ></BaseSelect >

      },
      render: (_: any, { category }: any) => (
        category && <Tag color={'#f50'}>{category.label}</Tag>
      ),
    },
    {
      title: '标签',
      dataIndex: 'tags',
      align:"center",
      key: 'tags',
      hideInSearch: true,
      render: (_: any, { tags }: any) => {
        return tags.map((item: any, index: number) => {
          return <Tag key={index} color="rgb(245, 34, 45)">{item.label}</Tag>
        })

      },
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      align:"center",
      key: 'views',
      hideInSearch: true,
      render: (_: any, { views }: any) => (
        <Space size="middle">
          <span className={style.views}>{views} </span>
        </Space>
      ),
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      align:"center",
      key: 'likes',
      hideInSearch: true,//在查询表单中不展示此项
      render: (_: any, record: any) => (
        <Space size="middle">
          <span className={style.likes}>{_} </span>
        </Space>
      ),
    },

    {
      title: '发布时间',
      dataIndex: 'publishAt',
      align:"center",
      key: 'publishAt',
      hideInSearch: true,//在查询表单中不展示此项
      render: (_: any, { publishAt }: any) => (
        <Space size="middle">
          <span>{date(publishAt)} </span>
        </Space>
      ),
    },
    {
      title: '操作',
      key: '操作',
      fixed: 'right',
      align:"center",
      hideInSearch: true,
      render: (_: any, record: any) => (
        <Space size="middle">
          <Button type="link" style={{ padding: "0", fontSize: "14px" }} onClick={()=>{
            history.push(`/editor/${record.id}`)
          }}>编辑</Button>

          <Button type="link" style={{ padding: "0", fontSize: "14px" }}
            onClick={() => {
              actionRef.current && actionRef.current.reload();
              _Recommended_article(record.id, !record.isRecommended)
            }}>{record.isRecommended ? "撤销首焦" : "首焦推荐"}</Button>
          <Button type="link" style={{ padding: "0", fontSize: "14px" }} onClick={() => { setIsModalOpen(true) }}>查看访问</Button>
          <Popconfirm title="确定删除这个文章吗" okText="确定" cancelText="取消"
            onConfirm={() => {
              actionRef.current && actionRef.current.reload();
              _del_article(record.id)
            }}>
            <Button type="link" style={{ padding: "0", fontSize: "14px" }} onClick={async () => {
            }}>删除</Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  console.log(hasSelected);

  return (
    <div className={style.article}>
      <div className={style.header}> 
       <Affix offsetTop={48}>
                <div className={style.article_header}>
                 <span onClick={(()=>history.push('/home/workbench'))}>工作台</span> <span>/ </span>  <span>文章管理</span>  
                </div>
            </Affix></div>
      <div className={style.main}>
        <ProTable
          actionRef={actionRef}
          rowSelection={rowSelection}
          rowKey={"id"}
          columns={columns} //列配置
          pagination={{
            defaultPageSize: 10,
            pageSizeOptions: [5, 10, 15, 20],
            showSizeChanger: true
          }}
          toolbar={{
            actions: [
              <Button type="primary" style={{ margin: "0 5px" }}  onClick={()=>{history.push('/create/artical')}}> + 新建</Button>
            ],
            title: [
              hasSelected ? <div>
                <Button style={{ margin: "0 5px" }} onClick={() => {
                  selectedRowKeys.forEach((item: any) => {
                    actionRef.current && actionRef.current.reload();
                    _set_article_status(item, "publish")
                  })
                }}>发布</Button>
                <Button style={{ margin: "0 5px" }} onClick={() => {
                  selectedRowKeys.forEach((item: any) => {
                    actionRef.current && actionRef.current.reload();
                    _set_article_status(item, "draft")
                  })
                }}>草稿</Button>
                <Button style={{ margin: "0 5px" }} onClick={() => {
                  selectedRowKeys.forEach((item: any) => {
                    actionRef.current && actionRef.current.reload();
                    _Recommended_article(item, true)
                  })
                }}>首焦推荐</Button>
                <Button style={{ margin: "0 5px" }} onClick={() => {
                  selectedRowKeys.forEach((item: any) => {
                    actionRef.current && actionRef.current.reload();
                    _Recommended_article(item, false)
                  })
                }}>撤销首焦</Button>
                <Popconfirm title="确定删除吗" okText="确定" cancelText="取消"
                  onConfirm={() => {
                    selectedRowKeys.forEach((item: any) => {
                      actionRef.current && actionRef.current.reload();
                      _del_article(item)
                    })
                  }}>
                  <Button style={{ margin: "0 5px" }} onClick={async () => {
                  }}>删除</Button>
                </Popconfirm>
              </div> : <div></div>
            ],
          }}

          request={
            async (options) => {
              console.log(options, "=====================");
              options = {
                ...options,
                page: options.current
              }
              delete options.current
              const { data } = await runAsync(options as any)
              return {
                data: data[0],
                success: true,
                total: data[1]
              }
            }
          }
        ></ProTable>
      </div>
      <MyModal handleOk={() => {
        setIsModalOpen(false)
      }} isModalOpen={isModalOpen} ></MyModal>
    </div>
  )
}

export default Article
