import React from 'react'
import styles from "./index.less"
import { SettingOutlined } from '@ant-design/icons';
import {history} from 'umi'
const index = () => {
  return (
    <div className={styles.wrap}>
        <div className={styles.mainleft}>
            <p>
                <span onClick={()=>{
                    history.push('/home/knowledge')
                }}>X</span><span></span><span><SettingOutlined/></span>
            </p>
            <button className={styles.btn}>保存</button>
            <div><span>0篇文章</span><span><button className={styles.btnn}>+ 新建 </button></span></div>
        </div>
        <div className={styles.mainright}>
             <p>请新建文章(或者选择章节进行编辑)</p>
        </div>
    </div>
  )
}

export default index