import React, { FC, useState, useRef, useEffect } from 'react';
import styles from "./index.less"
import { _getviewList, Visitlist, SearchviewList } from "@/api"
import { useRequest } from "ahooks"
import {
    Affix,
    Breadcrumb,
    Select,
    Table,
    Popconfirm,
    message,
    Button,
    Input,
    PaginationProps,
    Form, Switch
} from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Alert, Pagination } from 'antd';
type Props = {
    visitData: any,
    res: any[],
}
// 表格
interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
}
const { Option } = Select;
//访问统计页面 
const VisitPage = (_props: Props) => {
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    let arr = [10, 20, 50, 100];
    const [data, setData] = useState({
        list: [],
        total: 0,
    });
    //展示总条数
    const showTotal: PaginationProps['showTotal'] = (total) => `共 ${data.total} 条`;

    let [page, setPage] = useState(1);
    let [pageSize, setPageSize] = useState(10);

    const { runAsync } = useRequest(_getviewList, {
        manual: true
    })
    const [list, setlist] = useState([]);

    const SearchVisit = useRequest(SearchviewList, { manual: true })
    const visitdel = useRequest(Visitlist, {
        manual: true
    })

    const hasSelected = selectedRowKeys.length > 0;
    //获取列表数据
    const visitList = async ({ page, pageSize }: any) => {
        const res: any = await runAsync({ page, pageSize });
        console.log(res, 'resssss');

        setData({
            list: res.data[0],
            total: res.data[1],
        });

    };

    // const getlist = async ({ page, pageSize }: any) => {
    //     let res = await _getviewList({ page, pageSize });
    //     setlist(res.data);
    // };
    useEffect(() => {
        visitList({ page: 1, pageSize: 10 });
    }, []);
    // 分页改一页几条
    function onShowSizeChange(current: number, pageSize: number) {
        setPage(current);
        setPageSize(pageSize);
        visitList({ page, pageSize });
    }


    // // 删除
    const confirm = async (id: string) => {

        const res = await Visitlist(id)
        if (res.statusCode === 200) {
            location.reload()
            message.success('操作成功');
        } else {
            message.error('操作失败')
        }
    };
    // // 批量删除
    const delbatches = () => {
        selectedRowKeys.forEach(async (id) => {
            const res = await Visitlist(id)
            if (res.statusCode === 200) {
                location.reload()
                message.success('操作成功');
            } else {
                message.error('操作失败')
            }

        });
    }
    const cancel = (id: string) => {
        // console.log();
        // message.error('取消删除');
    };
    // 表格
    const columns: ColumnsType<DataType> = [
        {
            title: 'URL',
            dataIndex: 'url',
            key: 'url',
            fixed: 'left',
            render: (text, record: any) => [<a href='url'>{record.url}</a>]
        },
        {
            title: 'IP',
            dataIndex: 'ip',
            key: 'ip',
            fixed: 'left',
        },
        { title: '浏览器', dataIndex: 'browser', key: 'browser' },
        { title: '内核', dataIndex: 'engine', key: 'engine' },
        { title: '操作系统', dataIndex: 'os', key: 'os' },
        { title: '设备', dataIndex: 'device', key: 'device' },
        { title: '地址', dataIndex: 'address', key: 'address' },
        { title: '访问量', dataIndex: 'count', key: 'count' },
        { title: '访问时间', dataIndex: 'updateAt', key: 'updateAt' },
        {
            title: '操作',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (text, record: any) => <Popconfirm
                title="确认删除这个访问？"
                onConfirm={() => {
                    confirm(record.id)
                }}
                onCancel={() => {
                    cancel
                }}
                okText="确定"
                cancelText="取消"
            >
                <a href="#">删除</a>
            </Popconfirm>,
        },
    ];
    //多选
    function onSelectChange(newSelectedRowKeys: React.Key[]) {
        setSelectedRowKeys(newSelectedRowKeys);
    }
    //全选
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    const [form] = Form.useForm();
    const textInput = useRef(null) as any;
    //搜索
    const SearchBtn = () => {
        const values = textInput.current.input.value;
        // console.log(values);
        SearchVisit.runAsync({ page, pageSize, ip: values }).then(
            (res: any) => {
                // console.log(res)
                setData({
                    list: res.data[0],
                    total: res.data[1],
                });
            },
        );


    };
    //重置
    const onReset = () => {
        form.resetFields();
        // visitList();
    };
    return (
        <div id={styles.visit}>
            <Affix offsetTop={48} onChange={affixed => console.log(affixed)}>
                <Breadcrumb separator="/" className={styles.visit_header}>
                    <Breadcrumb.Item href="/home/workbench">工作台</Breadcrumb.Item>
                    <Breadcrumb.Item href="/home/system">访问统计</Breadcrumb.Item>
                </Breadcrumb>
            </Affix>

            {/* 搜索 */}
            <div className={styles.visit_input}>
                <div className={styles.visit_inputForm}>
                    <p>
                        <span className={styles.classfit_span}>IP:</span>
                        <Input placeholder="请输入IP地址" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>UA:</span>
                        <Input placeholder="请输入User Agent" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>URL:</span>
                        <Input placeholder="请输入URL" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>地址:</span>
                        <Input placeholder="请输入地址" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>浏览器:</span>
                        <Input placeholder="请输入浏览器" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>内核:</span>
                        <Input placeholder="请输入内核" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>OS:</span>
                        <Input placeholder="请输入操作系统" style={{ width: "200px" }} ref={textInput} />
                    </p>
                    <p>
                        <span className={styles.classfit_span}>设备:</span>
                        <Input placeholder="请输入设备" style={{ width: "200px" }} ref={textInput} />
                    </p>

                </div>
                <p className={styles.visit_buttonbox}>
                    <Button type="primary" onClick={SearchBtn}>搜索</Button>
                    <Button className={styles.search_btn} onClick={onReset} >重置</Button>
                </p>
            </div>
            <div>
                <span style={{ marginLeft: 8, width: '600px' }}>
                    {hasSelected ? (
                        <p className={styles.visit_button}>
                            <Button type="primary" danger ghost
                                onClick={delbatches}
                            >
                                删除
                            </Button>
                        </p>
                    ) : (
                        ''
                    )}
                </span>
            </div>
            {/* 表格 */}
            <div className={styles.visitTab}>
                <Table
                    rowSelection={rowSelection}
                    rowKey="id"
                    style={{ marginTop: '20px' }}
                    columns={columns}
                    dataSource={data.list}
                    scroll={{ x: 1500 }}
                    pagination={false}
                />
            </div>
            {/* 分页 */}
            <div className={styles.visitpage}>
                <Pagination
                    defaultCurrent={1}
                    total={data.total}
                    pageSizeOptions={arr}
                    // showSizeChanger={true}
                    defaultPageSize={pageSize}
                    onChange={onShowSizeChange}
                />
            </div>
        </div>
    )
}

export default VisitPage