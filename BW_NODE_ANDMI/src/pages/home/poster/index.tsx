import styles from "./index.less"
import {FC} from 'react'
import { EditOutlined, EllipsisOutlined, SettingOutlined, UndoOutlined,CloudUploadOutlined,DeleteOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import { useDispatch, useSelector } from "umi";
import { useEffect } from "react";
import { Switch, Table, Button, Popconfirm, Tag, Input, Select, Cascader } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { message, Upload } from 'antd';
import React from 'react';

const { Dragger } = Upload;
const { Meta } = Card;
import { history } from 'umi';

// 海报管理页面


const Poster:FC = () => {
    const { Option } = Select;
    const props: UploadProps = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info) {
          const { status } = info.file;
          if (status !== 'uploading') {
            console.log(info.file, info.fileList);
          }
          if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
          } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
        onDrop(e) {
          console.log('Dropped files', e.dataTransfer.files);
        },
      };
    
    return (
        <div>
            <div className={styles.allarticleWarp}>
            <div className={styles.nav}>
                <p onClick={() => {
                    history.push('/home/workbench')
                }}>工作台 / 海报管理</p>
            </div>
            <div className={styles.mainn}>
                <div className={styles.updata}>
                   {/* 上传文件  */}
                   <Dragger {...props} className={styles.Dragger}>
                    <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                    </p>
                    <p className={styles.ant}>点击选择文件获将文件拖拽到此处</p>
                    <p className={styles.ant}>文件将上传到OSS,如未配置请先配置</p>
                </Dragger>
                </div>
               
            </div>
            <div className={styles.main}>
                <div className={styles.search}>
                    {/* 搜索 */}

                    <p className='searchinput'><span className={styles.classfit_span}>文件名称:</span>  <Input placeholder="请输入文件名称" style={{ width: "240px" }} /></p>
                    <p className={styles.searchbtn}>
                        <Button type="primary">搜索</Button> <Button type="primary" className={styles.search_btn}>重置</Button>
                    </p>
                </div>
               
            </div>
            <div className={styles.nolist}></div>
        </div>
        </div>
    )
}

export default Poster