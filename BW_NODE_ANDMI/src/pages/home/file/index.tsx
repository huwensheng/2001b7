import styles from './index.less';
import type { PaginationProps } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { Drawer, Col, Row, DatePicker } from 'antd';
import Card from '../../../components/card';
import Draw from '../../../components/draw';
import { PlusOutlined } from '@ant-design/icons';
import { useRequest } from 'umi';
import { fileList } from '../../../api/jiaindex';
import UpLoad from '../../../components/upload';
import Breadcrumb from '../../../components/breadcrumb';
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';
import {
    Table,
    Tag,
    Space,
    Button,
    Form,
    Input,
    Checkbox,
    Select,
    Pagination,
    Switch,
} from 'antd';

export default function IndexPage() {
    const [page, setPage] = useState(1)
    const [Items, setItems] = useState({})
    const [pageSize, setPageSize] = useState(10)
    const [total, setTotal] = useState(0)
    //第一个输入框
    const textInput = useRef(null) as any;
    //第二个输入框
    const emailInp = useRef(null) as any;
    const { run } = useRequest(fileList, { manual: true });

    const [visible, setVisible] = useState(false);
    const [Data, setData] = useState([]);
    const [DataList, setDataList] = useState([]);
    const init = async (page: number, pageSize: number) => {
        let res = await run({ page, pageSize })
        // console.log(res, 'res');
        setData(res[0])
        setTotal(res[1])
    }

    //打开抽屉
    const showDrawer = (item: any) => {
        setItems(item);
        setVisible(true);
    };
    //关闭抽屉
    const onClose = () => {
        setVisible(false);
        setItems({});
    };

    useEffect(() => {
        init(page, pageSize)

    }, []);
    // 监听分页
    const change = (page: number, pageSize: number) => {
        setPage(page)
        setPageSize(pageSize)
        init(page, pageSize)
    }

    // 重置
    const resetBtn = () => {
        // console.log(seleInp.current);
        textInput.current.input.value = null;
        emailInp.current.input.value = null;
        // init(page, pageSize)
    }
    //搜索
    const SearchBtn = () => {
        const values = textInput.current.input.value;//文件名称
        const typeValue = emailInp.current.input.value;//文件类型
        run({ page, pageSize, originalname: values, type: typeValue }).then(res => {
            // console.log(res, '搜索res');
            setData(res[0])
            setTotal(res[1])
        })

    };
    // 分页，一共多少条数据
    const showTotal: PaginationProps['showTotal'] = total => `共 ${total} 条数据`;
    return (
        <div className={styles.knowledge_box}>
            <Breadcrumb children={{ title: '文件管理', path: '/files' }} />
            <div className={styles.head}>
                <UpLoad />
            </div>
            <div className={styles.header_box}>
                <div className={styles.input_box}>
                    文件名称:
                    <Input placeholder="请输入文件名称" style={{ width: '230px' }} ref={textInput} />
                    文件类型:
                    <Input placeholder="请输入文件类型" style={{ width: '230px' }} ref={emailInp} />
                </div>
                <div className={styles.button_box}>
                    <Button type="primary"
                        style={{ marginRight: '30px' }}
                        onClick={SearchBtn}
                    >
                        搜索
                    </Button>
                    <Button onClick={resetBtn}>重置</Button>
                </div>
            </div>

            <div className={styles.content_box}>
                {/* 新建 */}
                <div className={styles.content_head}>
                    <div></div>
                </div>
                <div className={styles.content}>
                    {Data.length &&
                        Data.map((item: any, index: number) => {
                            return (
                                <div key={index} className={styles.dl}>
                                    <dl onClick={() => showDrawer(item)}>
                                        <dt>
                                            <img
                                                src={item.url}
                                                alt=""
                                                style={{ width: 250, height: 180 }}
                                            />
                                        </dt>
                                        <dd>
                                            <p>{item.originalname}</p>
                                            <p>上传于{item.createAt}</p>
                                        </dd>
                                    </dl>
                                </div>
                            );
                        })}
                    {/* <Card
                        list={Data}
                        isAction={true}
                        onShow={showDrawer}
                    ></Card> */}

                    <div className={styles.footer_box}>
                        <div></div>
                        <Pagination
                            showTotal={showTotal}
                            total={total}
                            current={page}
                            pageSize={pageSize}
                            showSizeChanger={true}
                            pageSizeOptions={[10, 20, 50, 100]}
                            onChange={change}
                        />
                    </div>
                </div>
            </div>

            {/* 新建抽屉 */}
            <Draw visible={visible} Items={Items} changeShowDrawer={onClose} />
        </div>
    );
}
