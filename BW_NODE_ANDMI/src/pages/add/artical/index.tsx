import React, { useEffect, useState } from 'react'
import styles from './index.less'
import MDEditor, { commands } from '@uiw/react-md-editor';
import content from "@/config/defaultArticleCont"
import _ from 'lodash'

import { Popconfirm, Button, Menu, Dropdown, Drawer, Form, Input, InputNumber, message } from 'antd'
import { history } from 'umi'
import { _set_addPage, _set_pageEditFn } from '@/api/index';

type Props = {
}

const ArticalCraete = (props: Props) => {
    const [value, setvalue] = useState(() => content)
    const [formValue, setFormValue] = useState<any>({})
    const [name, setnameValue] = useState('')
    const [open, setOpen] = useState(false);
    const [form] = Form.useForm();
    const handleChange = (e: any) => {
        // console.log(e);
        setvalue(e)
    }
    useEffect(() => {
        form.setFieldsValue({
            path: "",
            name: "",
        })
    }, [form])
    const onFinish = (values: any) => {
        // console.log(values);
        values.cover = null
        values.name = name;
        values.content = content
        values.toc = "toc"
        values.html = "html"
        values.status = 'publish'

        setFormValue(values)
        // console.log(formValue);
        
        setOpen(false)
    };

    const DeleteOK = () => {
        // console.log(78);
        history.push('/page')
    }
    const publish = async () => {
        // console.log(formValue);
        if (JSON.stringify(formValue) === '{}') {
            message.error('请输入页面路径')
        } else {
            if (name === '') {
                message.error('请输入名称')
            } else {
                let res = await _set_addPage(formValue)
                console.log(res);
                if (res.statusCode === 400) {
                    history.push('/page/editor')
                    message.error(res.msg)

                } else if (res.statusCode == 201) {
                    message.success('添加成功')
                }
            }
        }
    }
    const showDrawer = () => {
        // setdatafile(item)
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };
    return (
        <div className={styles.con_editor}>
            <header className={styles.headers}>
                <div className={styles.head_left}>
                    <Popconfirm title="确认关闭？如果有内容变更，请先保存。" placement="rightBottom"
                        onConfirm={() => { DeleteOK() }} okText="确认" cancelText="取消">  <span className={styles.links}>x</span></Popconfirm>
                    <div>
                        <input defaultValue={''} onChange={(e) => {
                            // setFormValue(e.target.value)
                            setnameValue(e.target.value)
                            // console.log(e.target.value);

                        }} placeholder='请输入文章标题' type="text" name="" id="" />
                    </div>
                </div>
                <div className={styles.head_right}>
                    <div>
                        <Button type="primary" onClick={() => publish()}>发 布</Button>
                        <Dropdown overlay={<Menu>
                            <Menu.Item key='1'>查看</Menu.Item>
                            <Menu.Item key='2' onClick={(() => {
                                showDrawer()
                            })}>设置</Menu.Item>
                            <Menu.Item key='3'>保存草稿</Menu.Item>
                            <Menu.Item key='4'>删除</Menu.Item>
                        </Menu>}>
                            <span role="img" aria-label="ellipsis" className={styles.head_right_svg}><svg viewBox="64 64 896 896" focusable="false" data-icon="ellipsis" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path></svg></span>
                        </Dropdown>
                    </div>
                </div>
            </header>
            <main>
                <MDEditor
                    value={value}
                    onChange={_.throttle(handleChange, 300)}
                >
                    <MDEditor.Markdown source={value}></MDEditor.Markdown>
                </MDEditor>
            </main>
            {/*  */}
            <Drawer title="页面属性" placement="right" onClose={onClose} open={open} width='480' className={styles.drawer}>

                <Form form={form} onFinish={onFinish}>
                    <Form.Item label="封面">
                        <Input placeholder='请输入页面封面' />
                    </Form.Item>
                    <Form.Item name="path" label="路径">
                        <Input placeholder='请配置页面路径' />
                    </Form.Item>
                    <Form.Item label="顺序">
                        <InputNumber defaultValue='0' />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">确定</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </div>

    )
}

export default ArticalCraete