import "../../../global.css"
import './style.css'
import content from "@/config/defaultArticleCont"
import { FC, useEffect, useRef, useState } from 'react'
import { _to_article_Edit, _del_article, _get_tag, _edit_article, _new_article } from '@/api/index'
import { _get_category } from "@/api/article"
import { history, useParams } from 'umi'
// @ts-ignore
import MDEditor from '@uiw/react-md-editor';
import style from "./index.less"
import { Input, Button, Popover, Popconfirm, message, Modal, Drawer, Form, Dropdown, Menu } from 'antd';
import { SmallDashOutlined, ExclamationCircleOutlined, SmileOutlined, CloseOutlined } from '@ant-design/icons';
import { useRequest } from 'ahooks'
import EditorDrawer from "@/components/editorDrawer"
import _ from "lodash"
type Props = {}

interface dataType {
    id: string
    title: string | number | readonly string[] | undefined
    cover?: string | null
    summary?: string | null
    content?: string | null
    html?: string | null
    toc?: string | null
    category?: any | null
    tags?: any | null
    status?: string | null
    views?: number | null
    likes?: number | null
    isRecommended?: boolean | null
    password?: string | null
    needPassword?: boolean | null
    totalAmount?: string | null
    isPay?: boolean | null
    isCommentable?: boolean | null
    publishAt?: string | null
    createAt?: string | null
    updateAt?: string | null
}

interface MenuDataItem {
    title: string,
    type: string,
    level: number,
    key: number
}

const Editor: FC = (props: Props) => {
    const params: { id: string } = useParams()//获取id
    const [value, setValue] = useState("") //编辑md
    const [Myform] = Form.useForm(); //title表单
    const [currentData, setCurrentData] = useState<dataType>({})//页面数据
    const { data: categoryData } = useRequest(_get_category)//获取category
    const { data: tagData } = useRequest(_get_tag)//获取tag数据
    const [open, setOpen] = useState(false);//控制抽屉
    const [newData, setNewData] = useState<dataType>({}) // 抽屉修改后的数据
    const [newTitle, setNewTitle] = useState(currentData.title) //修改后的标题
    // ________________新建________________//
    const [curcontent, setCurcontent] = useState(() => content)
    const [currentTag, getCurrentTag] = useState("")
    const [menuData, setMenuData] = useState<MenuDataItem[]>();
    const preview = useRef<any>();
    const [Hdata, setHdata] = useState<any>([])
    const formatMenuData = () => {
        const previewEl = preview.current.querySelector('.w-md-editor-preview');
        // previewEl.style.paddingBottom = "800px"
        // const menuDataEl = Array.from(previewEl.querySelectorAll("*")).filter((item:any) => /^H[1-6]$/.test(item.nodeName));
        const menuDataEl = Array.from(previewEl.querySelectorAll("*")).filter((item:any) => /^H[1-6]$/.test(item.nodeName))
        setHdata(menuDataEl)
        setMenuData(menuDataEl.map<MenuDataItem>((item: any, index: number) => (
            {
                title: item.innerText,
                type: item.nodeName,
                level: item.nodeName.slice(1) * 1,
                key: index
            }
        )));
    };
    useEffect(() => {
        if(params.id){
            (async () => {
                const { data } = await _to_article_Edit(params.id)
                setValue(data.content)
                setCurrentData(data)
                Myform.setFieldsValue(data)
            })()
        }
    }, [])

    // 格式化tag

    useEffect(() => {
        setTimeout(() => {
            if (preview.current && value) {
                formatMenuData();//重新格式化标题
            }
        }, 0)
    }, [preview, value]);


    // 筛选标签
    let str = ""
    newData.tags && newData.tags.forEach((tag: any) => {
        str += `${tagData?.data.filter((item: any) => item.label === tag)[0]?.id},`
    })
    // 向子组件传的数据
    const EditorDrawerData = {
        categoryData,
        tagData,
        open,
        onClose: () => setOpen(false),
        currentData
    }

    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: (<span> 查看</span>),
                    disabled: params.id ? false : true,
                },
                {
                    key: '2',
                    label: (<span onClick={() => setOpen(true)}> 设置</span>),

                },
                {
                    key: '3',
                    label: (<span onClick={() => {
                        if (params.id) { //编辑
                            _edit_article(currentData.id, {
                                ...currentData,
                                ...newData,
                                title: newTitle,
                                content: value,
                                category: categoryData?.data.filter((item: any) => item.label === newData?.category)[0]?.id,
                                tags: str.substring(0, str.length - 1),
                                status: "draft"
                            })
                            message.success("文章已保存为草稿")
                        } else { //新建
                            _new_article({
                                ...newData,
                                content: curcontent,
                                title: newTitle,
                                status: "draft",
                            })
                            message.success("文章已保存为草稿")
                        }
                    }}> 保存草稿</span>),
                },
                {
                    key: '4',
                    label: (<span onClick={() => {
                        Modal.confirm({
                            title: '确认删除?',
                            icon: <ExclamationCircleOutlined />,
                            content: '删除内容后,无法恢复。',
                            okText: '确认',
                            cancelText: '取消',
                            onOk() {
                                _del_article(currentData.id)
                                setOpen(false);
                                history.go(-1)
                            },
                        });
                    }}> 删除</span>),
                    disabled: params.id ? false : true,
                },
            ]}
        />
    );
    return (
        <div className={style.editor}>
            {
                params.id
                    // 编辑内容
                    ? <>
                        <div className={style.editor_top}>
                            <div className={style.editor_top_left}>
                                <Popconfirm
                                    title="确认关闭？如果有内容变更，请先保存。"
                                    okText="确定" cancelText="取消"
                                    onConfirm={() => {
                                        history.go(-1)
                                    }}
                                >
                                    <span><b>X</b></span>
                                </Popconfirm>
                                <Form form={Myform}>
                                    <Form.Item name="title">
                                        <Input onChange={(e) => {
                                            setNewTitle(e.target.value)
                                        }} />
                                    </Form.Item>
                                </Form>
                            </div>
                            <div>
                                <Button type="primary" onClick={async () => {
                                    const res = await _edit_article(currentData.id, {
                                        ...currentData,
                                        ...newData,
                                        content: value,
                                        title: newTitle,
                                        category: categoryData?.data.filter((item: any) => item.label === newData?.category)[0]?.id,
                                        tags: str.substring(0, str.length - 1),
                                        // cover: ""
                                    })
                                    if (res.statusCode === 200) {
                                        message.success("发布文章成功")
                                    } else if (res.statusCode === 500) {
                                        message.warning("未知错误")
                                    }
                                }}>发布</Button> &emsp;
                                <Dropdown overlay={menu} placement="bottom" arrow={{ pointAtCenter: true }}>
                                    <SmallDashOutlined style={{ color: "#1890FF" }} />
                                </Dropdown>
                            </div>
                        </div>
                        <div className={style.Outlinewrap}>
                            <MDEditor
                                value={value}
                                onChange={_.throttle((e: any) => {
                                    setValue(e)
                                }, 300)}
                                ref={(val: { container: any }) => {
                                    if (val) {
                                        preview.current = val.container;
                                    }
                                }}
                            >
                                <MDEditor.Markdown source={value} />
                            </MDEditor>
                            <div className={style.Outline}>
                                <div className={style.Outline_head}>
                                    <b>大纲</b>
                                    <span><CloseOutlined /></span>
                                </div>
                                <div className={style.Outline_title}>
                                    {
                                        menuData && menuData.map((item, index) => {
                                            return <li key={item.key} style={{ marginLeft: item.level * 6 }} onClick={() => {
                                                Hdata[index].scrollIntoView({ behavior: "smooth" })
                                            }}>
                                                {item.title}
                                            </li>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        <EditorDrawer EditorDrawerData={EditorDrawerData} formData={(e: any) => {
                            setNewData(e)
                        }}></EditorDrawer>
                    </>
                    // 新建内容
                    : <>
                        <div className={style.editor_top}>
                            <div className={style.editor_top_left}>
                                <Popconfirm
                                    title="确认关闭？如果有内容变更，请先保存。"
                                    okText="确定" cancelText="取消"
                                    onConfirm={() => {
                                        history.go(-1)
                                    }}
                                >
                                    <span><b>X</b></span>
                                </Popconfirm>
                                <Form form={Myform} >
                                    <Form.Item name="title">
                                        <Input onChange={(e) => {
                                            setNewTitle(e.target.value)
                                        }} />
                                    </Form.Item>
                                </Form>
                            </div>
                            <div>
                                <Button type="primary" onClick={async () => {
                                    if (newTitle) {
                                        const res = await _new_article({
                                            ...newData,
                                            content: curcontent,
                                            title: newTitle,
                                            status: "publish",
                                            tags: str.substring(0, str.length - 1)
                                        })
                                        if (res.statusCode === 201) {
                                            message.success("发布文章成功")
                                        } else if (res.statusCode === 500) {
                                            message.warning("未知错误")
                                        }
                                    } else {
                                        message.warning("请输入文章标题")
                                    }
                                }}>发布</Button> &emsp;
                                <Dropdown overlay={menu} placement="bottom" arrow={{ pointAtCenter: true }}>
                                    <SmallDashOutlined style={{ color: "#1890FF" }} />
                                </Dropdown>
                            </div>
                        </div>
                        <div className={style.Outlinewrap}>
                            <MDEditor
                                value={curcontent}
                                onChange={_.throttle((e: any) => {
                                    setCurcontent(e)
                                }, 300)}
                                ref={(val: { container: any }) => {
                                    if (val) {
                                        preview.current = val.container;
                                    }
                                }}
                            >
                                <MDEditor.Markdown source={curcontent} />
                            </MDEditor>
                            <div className={style.Outline}>
                                <div className={style.Outline_head}>
                                    <b>大纲</b>
                                    <span><CloseOutlined /></span>
                                </div>
                                <div className={style.Outline_title}>
                                    {
                                        menuData && menuData.map((item, index) => {
                                            return <li key={item.key} style={{ marginLeft: item.level * 6 }} onClick={() => {
                                                Hdata[index].scrollIntoView({ behavior: "smooth" })
                                            }}>
                                                {item.title}
                                            </li>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        <EditorDrawer EditorDrawerData={EditorDrawerData} formData={(e: any) => {
                            setNewData(e)
                        }}></EditorDrawer>
                    </>
            }
        </div >
    )
}

export default Editor