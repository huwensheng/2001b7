import { FC, useEffect, useState } from 'react'
import "./style.css"

type Props = {}

const AllEditor:FC = () => {
    const [html, setHtml] = useState<any>('');
    useEffect(() => {
        // 监听子应用传递过来的数据
        window.addEventListener("message", function (msg:any) {
            console.log(msg);
            setHtml(msg)
        })
    }, [])
    const handleSubm= ()=>{
        // 发起http请求
        console.log(html);
        
    }
    return (
        <div className='mded'>
            <button onClick={handleSubm}>发送</button>
            {/* 嵌套子页面 */}
            <iframe src="http://jasonandjay.com/editor/static/" width={"100%"} height={700}></iframe>
        </div>
    )
}

export default AllEditor