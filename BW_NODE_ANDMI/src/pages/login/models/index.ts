// import {_get_artical} from "@/api"
// export interface loginState {
//     name:number;
//     articleList:any[]
// }
// import{Reducer,Effect} from "umi"


// interface loginModelInterface{
//   namespace:string;
//   state:loginState;
//   reducers:{
//     changeList:Reducer<loginState>;
//   };
//   effects:{
//     getArtical:Effect
//   }
// }
// const loginModel:loginModelInterface={ 
//     namespace:"loginModel",
//     state:{
//         name:0,
//         articleList:[] //第一步创建空数组
//     },
//     reducers:{
//         changeList(state,{payload}){ //第三不
//             return {
//                 ...state,
//                 articleList:[...payload] //空数组就是等于数据
//             } as loginState
//         }
//     },
//    effects:{ //第二部异步请求
//     *getArtical (__,{put,call}){
//         const data:{data:any[]}=yield call(_get_artical)
//         yield put({
//             type:"changeList", //根据它的红色的字传给上面
//             payload:[...data.data[0]]
//         })
//     }
//    }
// }
// export default loginModel