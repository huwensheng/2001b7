// request 统一配置
// 统一处理请求公共参数 携带token 身份标识  csrf-token 
// 错误的统一处理


import { RequestConfig, history } from "umi"
import { Button, Dropdown, Menu, message } from "antd"
import {
    UserOutlined,
    GithubOutlined,
} from '@ant-design/icons';
  

interface Headers {
    authorization?: string | null | undefined,
    [propName: string]: any
}

// 启用initial-state插件
export const getInitialState = () => { // 该函数在页面初始化时 执行一次 而且只执行一次
    //容错处理
    try {
        const userInfo = JSON.parse(localStorage.getItem("userInfo") as string)//获取本地存储的用户信息  必须要用JSON给他从字符串转为对象或数组数据
        if (!userInfo) {
            history.replace('/login')
        }
        return userInfo
    } catch (error) {
        console.log(error);
    }
}


// layout
export const layout = () => {
    const menu = (
        <Menu items={
            [
                {
                    key: '/create/editor',
                    label: '新建文章-协同编辑器'
                },
                {
                    key: '/create/artical',
                    label: '新建文章'
                },
                {
                    key: '/create/page',
                    label: '新建页面'
                }
            ]
        }
            onClick={({ key, domEvent }) => {

                // 获取原生事件
                domEvent.stopPropagation()//阻止冒泡
                history.push(key)//跳转到key路径
            }}
        >
        </Menu>
    )
    const setmenu = (
        <Menu
            onClick={({ key, domEvent }) => {
                // domEvent 获取原生事件
                domEvent.stopPropagation(); // 阻止事件的冒泡行为；
                history.push(key);
                if (key === "/login") localStorage.clear()
            }}
            items={[
                {
                    key: '/home/personal',
                    label: '个人中心',
                },
                {
                    key: '/home/user',
                    label: '用户管理',
                },
                {
                    key: '/home/system',
                    label: '系统设置',
                },
                {
                    key: '/login',
                    label: '退出登录',
                },
            ]}
        ></Menu>
    );

    return {
        menuHeaderRender: (logo: string, title: string) => {
            return <div style={{ width: "100%", textAlign: 'center' }}>
                <div>
                    {logo}
                    {title}
                </div>
                <Dropdown overlay={menu} placement="bottom">
                    <Button style={{ fontSize: 18, marginTop: 10, width: 180, height: 40, background: "#0188fb", borderColor: '#0188fb', color: '#ffffff' }}>

                        + 新增
                    </Button>
                </Dropdown>
            </div>
        },
        rightContentRender: () => (
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <GithubOutlined style={{ fontSize: '18px' }} />
                <Dropdown overlay={setmenu} placement="bottom">
                    <p
                        style={{
                            height: '100%',
                            margin: '0',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <UserOutlined
                            style={{
                                fontSize: '14px',
                                background: '#eee',
                                padding: '3px',
                                borderRadius: '50%',
                                margin: '5px',
                            }}
                        />
                        <span>
                            Hi,{localStorage.getItem("userInfo") && JSON.parse(localStorage.getItem('userInfo') as string).name}
                        </span>
                    </p>
                </Dropdown>
            </div>
        ),
    }
}

export const request: RequestConfig = {
    timeout: 20000,
    errorConfig: {
        adaptor: (resData:any) => {
            if(resData.statusCode === 200||201){
                 message.success(resData.msg)
            }else{
                message.error(resData.msg)
            }
            console.log(`修改相应结果adaptor`, resData);
            return {
                ...resData,
                errorMessage: resData.mag//success为false时用弹框提示errorMessage信息
            }
        }
    },
    requestInterceptors: [
        //请求拦截器
        (url: string, options: any) => {
            console.log(`接口请求前执行，处理请求接口时携带的公共参数`, options);
            const headers: Headers = {
                ...options.headers
            }
            // 判断权限有没有
            if (!options.isAuthorization) {//获取本地存储中的token
                headers["authorization"] = `Bearer ${JSON.parse(localStorage.getItem('userInfo') as string).token}`
            }
            return {
                url: `${url}`,
                options: {
                    ...options,
                    interceptors: true,
                    headers
                }
            }

        }
    ],
    responseInterceptors: [
        async (response:any) => {
            const res = await response.json()
            if (res.statusCode === 403) {
                res.msgm = `您暂时没有该接口权限`
            }
            if (res.statusCode === 401) {
                history.replace('/login')
            }
            return res
        }
    ]
}