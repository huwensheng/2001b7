


import { useCallback, useEffect, useState } from 'react'

const workChart = () => {

    const [size, setSize] = useState({
        width: document.documentElement.clientWidth
    })
    const getSize = useCallback(() => {
        setSize({
            width: document.documentElement.clientWidth
        })
    }, [])
    useEffect(() => {
        window.addEventListener("resize", getSize)
        return () => {
            window.removeEventListener("resize", getSize)
        }
    }, [])
return size
}
export default workChart