import { RightContent } from '@ant-design/pro-layout/lib/components/TopNavHeader';
import { defineConfig } from 'umi';
import routes from './routes';

// 用来定义配置的
// .umirc.ts的优先级 要比config要高
export default defineConfig({
    title:"创作平台后台管理",
    routes,
    layout:{
        title:"八唯创作平台",
        logo:'https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif',
        collapsedWidth:80,
        siderWidth:210,
        
    },
    proxy:{
        "/api":{
            target:"https://creationapi.shbwyz.com",
            changeOrigin:true,
            pathRewrite:{
                "^/api":""
            }
        }
    },
    dva:{
        immer:true,
        hmr:true
    }
});