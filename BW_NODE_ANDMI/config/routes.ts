const routes = [
    {
        path: '/',
        redirect:'/home/workbench',

    },
    {
        path:'/login',
        component:"@/pages/login/index.tsx",
        headerRender:false, //是否渲染header
        menuRender:false ,//是否渲染menu
        isAuthorization:false
    },
    {
        path:'/registry',
        target:"_blank",//新的页面打开
        component:"@/pages/registry/index.tsx",
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'/compile',
        component:"@/pages/compile/index.tsx",
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'create/artical',//新建文章
        component:'@/pages/add/page/index.tsx',
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'/create/page',//新建页面
        component:'@/pages/add/artical/index.tsx',
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'/create/editor',
        component:'@/pages/add/mdeditor/index.tsx',
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'/editor/:id',
        component:'@/pages/add/editor/index.tsx',
        headerRender:false, //是否渲染header
        menuRender:false //是否渲染menu
    },
    {
        path:'/home/workbench',
        name:'工作台',
        component:"@/pages/home/workbench/index.tsx",
        icon:'dashboard'
    },
    {
        path:'/home/article',
        name:'文章管理',
        icon:'form',
        routes:[
            {
                path:'/home/article/allarticle',
                name:"所有文章",
                component:"@/pages/home/allarticle/index.tsx",
                icon:'form'
            },
            {
                path:'/home/article/classfiy',
                name:"分类管理",
                component:"@/pages/home/classfiy/index.tsx",
                icon:'copy',
            },
            {
                path:'/home/article/label',
                name:"标签管理",
                component:"@/pages/home/label/index.tsx",
                icon:'tag',
            }
        ]
    },
    {
        path:'/home/page',
        name:'页面管理',
        component:"@/pages/home/page/index.tsx",
        icon:'snippets',
    },
    {
        path:'/home/knowledge',
        name:'知识小册',
        component:"@/pages/home/knowledge/index.tsx",
        icon:'book',
    },
    {
        path:'/home/poster',
        name:'海报管理',
        component:"@/pages/home/poster/index.tsx",
        icon:'star',
    },
    {
        path:'/home/comment',
        name:'评论管理',
        component:"@/pages/home/comment/index.tsx",
        icon:'message',
    },
    {
        path:'/home/email',
        name:'邮件管理',
        component:"@/pages/home/email/index.tsx",
        icon:'mail',
    },
    {
        path:'/home/file',
        name:'文件管理',
        component:"@/pages/home/file/index.tsx",
        icon:'folder-open',
    },
    {
        path:'/home/search',
        name:'搜索记录',
        component:"@/pages/home/search/index.tsx",
        icon:'search',
    },
    {
        path:'/home/visit',
        name:'访问统计',
        component:"@/pages/home/visit/index.tsx",
        icon:'project',
    },
    {
        path:'/home/user',
        name:'用户管理',
        component:"@/pages/home/user/index.tsx",
        access:"isAdmin", //在渲染组件前判断 access.isAdmin是否为true
        icon:'user',
    },
    {
        path:'/home/system',
        name:'系统设置',
        component:"@/pages/home/system/index.tsx",
        icon:'setting',
    },
    {
        path:'/home/personal',
        component:"@/pages/home/personal/index.tsx",
    },
    {
        path:'/home/knowdetails:id',
        component:"@/pages/home/knowdetails/index.tsx",
        headerRender:false, //是否渲染header
        menuRender:false ,//是否渲染menu
    }
]

export default routes